# nWidart commands
## Documentation: 
`https://docs.laravelmodules.com/`
## Module Commands:
Generate a new module. <br>
`php artisan module:make Blog`

Generate multiple modules at once.<br>
`php artisan module:make Blog User Auth`

Use a given module. This allows you to not specify the module name on other commands requiring the module name as an argument.<br>
`php artisan module:use Blog`

This unsets the specified module that was set with the module:use command.<br>
`php artisan module:unuse`

Enable/Disable the given module.<br>
`php artisan module:enable Blog`<br>
`php artisan module:disable Blog`

List all available modules.<br>
`php artisan module:list`

### Migrations
Migrate the given module, or without a module an argument, migrate all modules.<br>
`php artisan module:migrate Blog`<br>
`php artisan module:migrate-rollback Blog`<br>
`php artisan module:migrate-refresh Blog`<br>
`php artisan module:migrate-reset Blog`<br>
`php artisan module:seed Blog`<br>
`php artisan module:publish-migration Blog`<br>

### Generator commands
Generate the given console command for the specified module.<br>
`php artisan module:make-command CreatePostCommand Blog`

Generate a migration for specified module.<br>
`php artisan module:make-migration create_posts_table Blog`

Generate the given seed name for the specified module.
`php artisan module:make-seed seed_fake_blog_posts Blog`

### Controller Generator
Generate a controller for the specified module.
`php artisan module:make-controller PostsController Blog`

create a plain controller
`php artisan module:make-controller --plain PostsController Blog`

create a resouce controller
`php artisan module:make-controller --api PostsController Blog`

### Model Generator
Generate the given model for the specified module.<br>
`php artisan module:make-model Post Blog`

Optional options:<br>
* `--fillable=field1,field2`: set the fillable fields on the generated model<br>
* `--migration, -m`: create the migration file for the given model

### Provider Generator
`php artisan module:make-provider BlogServiceProvider Blog`

### Middleware Generator
`php artisan module:make-middleware CanReadPostsMiddleware Blog`

### E-Mail Generator
`php artisan module:make-mail SendWeeklyPostsEmail Blog`

### Notification Generator
`php artisan module:make-notification NotifyAdminOfNewComment Blog`

### Listener Generator:
`php artisan module:make-listener NotifyUsersOfANewPost Blog`<br>
`php artisan module:make-listener NotifyUsersOfANewPost Blog --event=PostWasCreated`<br>
`php artisan module:make-listener NotifyUsersOfANewPost Blog --event=PostWasCreated --queued`<br>

### Request Generator
`php artisan module:make-request CreatePostRequest Blog`

### Event Generator
`php artisan module:make-event BlogPostWasUpdated Blog`

### Job Generator
`php artisan module:make-job JobName Blog`<br>
`php artisan module:make-job JobName Blog --sync # A synchronous job class`

### Route Provider Generator
`php artisan module:route-provider Blog`

### Policy Generator
`php artisan module:make-policy PolicyName Blog`

### Rule Generator
`php artisan module:make-rule ValidationRule Blog`

### Resource Generator
`php artisan module:make-resource PostResource Blog`<br>
`php artisan module:make-resource PostResource Blog --collection`

## Create blog module example:
* php artisan dump:autoload
* First let's create the Blog module:<br>
`php artisan module:make Blog`<br>
* Let's create the Blog model with its migration file [-m]:<br>
`php artisan module:make-model -m Blog Blog`
* Let's create the user Module:<br>
`php artisan module:make User`<br>
`php artisan module:make-model -m User User`
* Let's create the requests:<br>
`php artisan module:make-request SaveBlogRequest Blog`
`php artisan module:make-request SaveUserRequest User`
* Let's create the policy file
`php artisan module:make-policy BlogPolicy Blog`
