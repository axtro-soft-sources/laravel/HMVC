<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\User\Entities\UserDefinition;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(UserDefinition::ID);
            $table->string(UserDefinition::FIRST_NAME, 200);
            $table->string(UserDefinition::LAST_NAME, 200);
            $table->string(UserDefinition::EMAIL, 200);
            $table->string(UserDefinition::PASSWORD, 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserDefinition::TABLE_NAME);
    }
};
