<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;

class User extends Model
{
    use HasFactory;

    protected $table = UserDefinition::TABLE_NAME;
    protected $fillable = UserDefinition::FILLABLES;

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserFactory::new();
    }

    public function blogs(): HasMany{
        return $this->hasMany(Blog::class, BlogDefinition::CREATED_BY, UserDefinition::ID);
    }
}
