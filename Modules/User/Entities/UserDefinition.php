<?php

namespace Modules\User\Entities;

use App\Entities\BaseFields;

class UserDefinition{
    const TABLE_NAME = 'user';
    const ID = BaseFields::ID;
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const EMAIL = 'email';
    const PASSWORD = 'passowrd';

    const FILLABLES = [
        self::FIRST_NAME,
        self::LAST_NAME,
        self::EMAIL,
        self::PASSWORD
    ];

    const SORTABLE = [
        'id' => self::ID,
        'first_name' => self::FIRST_NAME,
        'email' => self::EMAIL
    ];
}
