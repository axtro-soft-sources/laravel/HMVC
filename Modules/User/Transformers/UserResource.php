<?php

namespace Modules\User\Transformers;

use App\Transformers\BaseResource;
use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Modules\User\Entities\UserDefinition;

class UserResource extends BaseResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            'first_name' => $this[UserDefinition::FIRST_NAME],
            'last_name' => $this[UserDefinition::LAST_NAME],
            'email' => $this[UserDefinition::EMAIL]
        ]);
    }
}
