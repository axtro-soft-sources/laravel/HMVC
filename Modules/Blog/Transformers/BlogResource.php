<?php

namespace Modules\Blog\Transformers;

use App\Transformers\BaseResource;
use Illuminate\Http\Request;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;
use Modules\User\Transformers\userResource;

class BlogResource extends BaseResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            BlogDefinition::ID => $this[BlogDefinition::ID],
            BlogDefinition::TITLE => $this[BlogDefinition::TITLE],
            BlogDefinition::DESCRIPTION => $this[BlogDefinition::DESCRIPTION],
            BlogDefinition::CREATED_BY => new UserResource($this[BlogDefinition::CREATED_BY])
        ]);
    }
}
