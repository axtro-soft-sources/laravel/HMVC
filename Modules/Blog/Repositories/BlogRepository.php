<?php

namespace Modules\Blog\Repositories;

use App\Repositories\BaseRepository;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class BlogRepository extends BaseRepository
{
    /**
     * BlogRepository constructor.
     * @param $persistentClass
     * @param string[] $defaultOrder
     */
    public function __construct($persistentClass, $defaultOrder = ['id' => 'desc'])
    {
        parent::__construct($persistentClass, $defaultOrder);
    }

    /**
     * @param string|null $searchText
     * @param array $orderBy
     * @param bool $includeDefused
     * @return Builder[]|Collection
     */
    public function query(?string $searchText, $orderBy = array(), bool $includeDefused = false)
    {
        return $this->queryInternal($searchText, $orderBy, $includeDefused)->get();
    }

    /**
     * @param string|null $searchText
     * @param array $orderBy
     * @param bool $includeDefused
     * @return Builder
     */
    private function queryInternal(?string $searchText, $orderBy = array(), bool $includeDefused = false): Builder
    {
        $query = Blog::query();
        if ($includeDefused)
            $query = Blog::withDefused();

        $query->with(['createdBy']);

        if ($searchText) {
            $query->where(function (Builder $innerQuery) use ($searchText) {
                $innerQuery->where(BlogDefinition::TITLE, 'LIKE', "%$searchText%");
                $innerQuery->where(BlogDefinition::DESCRIPTION, 'LIKE', "%$searchText%");
            });
        }

        $this->buildNestedOrder($query, $orderBy);

        return $query;
    }

    /**
     * @param string|null $searchText
     * @param int $pageSize
     * @param array $orderBy
     * @param bool $includeDefused
     * @return LengthAwarePaginator
     */
    public function queryPaginated(?string $searchText, $pageSize = 15, $orderBy = array(), bool $includeDefused = false): LengthAwarePaginator
    {
        return $this->queryInternal($searchText, $orderBy, $includeDefused)->paginate($pageSize);
    }
}
