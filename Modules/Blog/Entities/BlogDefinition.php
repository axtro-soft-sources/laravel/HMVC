<?php

namespace Modules\Blog\Entities;

use App\Entities\BaseFields;

class BlogDefinition{
    const TABLE_NAME = 'blog';
    const ID = BaseFields::ID;
    const TITLE = BaseFields::TITLE;
    const DESCRIPTION = BaseFields::DESCRIPTION;
    const VIEWS = 'views';
    const CREATED_BY = 'created_by';

    const FILLABLES = [
        self::TITLE,
        self::DESCRIPTION,
        self::VIEWS
    ];

    const DEFAULT_SORT = 'name';

    const SORTABLES = [
        'id' => self::ID,
        'title' => self::TITLE
    ];
}
