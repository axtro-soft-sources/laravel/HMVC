<?php


namespace Modules\Blog\Entities;


use App\Entities\BasePermissions;

class BlogPermissions extends BasePermissions
{
    const LIST = 'blog.list';
    const ADD = 'blog.add';
    const GET = 'blog.get';
    const EDIT = 'blog.edit';
    const DELETE = 'blog.delete';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST,
            self::ADD,
            self::GET,
            self::EDIT,
            self::DELETE
        ];
    }
}
