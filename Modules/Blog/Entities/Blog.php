<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\User\Entities\UserDefinition;

class Blog extends Model
{
    use HasFactory;

    protected $table = BlogDefinition::TABLE_NAME;
    protected $fillable = BlogDefinition::FILLABLES;

    protected static function newFactory()
    {
        return \Modules\Blog\Database\factories\BlogFactory::new();
    }

    public function createdBy(): BelongsTo{
        return $this->belongsTo(User::class, BlogDefinition::CREATED_BY, UserDefinition::ID);
    }
}
