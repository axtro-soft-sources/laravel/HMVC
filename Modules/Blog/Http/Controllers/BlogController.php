<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Services\ServiceHelper;
use App\Transformers\PaginationResource;
use Illuminate\Support\Facades\Config;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;
use Modules\Blog\Http\Requests\ListBlogRequest;
use Modules\Blog\Http\Requests\SaveBlogRequest;
use Modules\Blog\Transformers\BlogResource;

class BlogController extends BaseController
{
    /** @var BlogService */
    private $service;

    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    /**
     * @param ListBlogRequest $request
     * @return PaginationResource|\Illuminate\Http\JsonResponse
     */
    public function index(ListBlogRequest $request)
    {
        $this->authorize('viewAny', Blog::class);
        $query = $request->get('query');
        $pageSize = $request->get('page_size', Config::get('blog.defaultPageSize'));
        $orderBy = $request->get('order_by', null);
        $orderDir = $request->get('order_dir', null);
        $paginate = $request->get('paginate', true);
        $includeDefused = $request->get('include_defused', false);
        if (empty($orderBy))
            $orderBy = BlogDefinition::DEFAULT_SORT;
        else
            $orderBy = BlogDefinition::SORTABLES[$orderBy];

        if (empty($orderDir))
            $orderDir = 'asc';

        if ($paginate) {
            $blogs = $this->service->queryPaginated($query, $pageSize, [$orderBy => $orderDir], $includeDefused);
            return new PaginationResource(BlogResource::class, $blogs);
        } else {
            $blogs = $this->service->query($query, [$orderBy => $orderDir], $includeDefused);
            return $this->ok(BlogResource::collection($blogs));
        }
    }

    /**
     * @param SaveBlogRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveBlogRequest $request)
    {
        $this->authorize('add', Blog::class);
        $created = $this->service->create([
            BlogDefinition::TITLE => $request->get(BlogDefinition::TITLE),
            BlogDefinition::DESCRIPTION => $request->get(BlogDefinition::DESCRIPTION),
            BlogDefinition::VIEWS => $request->get(BlogDefinition::VIEWS)
        ]);

        return $this->ok(new BlogResource($created));
    }

    /**
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Blog $blog)
    {
        $this->authorize('view', $blog);

        return $this->ok(new BlogResource($blog));
    }

    /**
     * @param SaveBlogRequest $request
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SaveBlogRequest $request, Blog $blog)
    {
        $this->authorize('update', $blog);

        $blog->fill([
            BlogDefinition::TITLE => $request->get(BlogDefinition::TITLE),
            BlogDefinition::DESCRIPTION => $request->get(BlogDefinition::DESCRIPTION),
            BlogDefinition::VIEWS => $request->get(BlogDefinition::VIEWS)
        ]);

        $this->service->save($blog);
        return $this->ok(new BlogResource($blog));
    }

    /**
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Blog $blog)
    {
        $this->authorize('delete', $blog);

        $this->service->softDeleteObject($blog);

        return $this->deleted();
    }
}
