<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;

class ListBlogRequest extends FormRequest
{
    protected $queryParametersToValidate = [
        'query',
        'page',
        'page_size',
        'order_by',
        'order_dir'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['paginate'] = 'nullable|boolean';

        return $rules;
    }

    public function isSortable()
    {
        return true;
    }

    public function getSortables()
    {
        return array_keys(BlogDefinition::SORTABLES);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('viewAny', Blog::class);
    }
}
