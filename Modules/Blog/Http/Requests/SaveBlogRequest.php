<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogDefinition;
use Modules\User\Entities\UserDefinition;

class SaveBlogRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            BlogDefinition::TITLE => ['required', 'string'],
            BlogDefinition::DESCRIPTION => ['required', 'string'],
            BlogDefinition::VIEWS => ['nullable', 'numeric'],
            BlogDefinition::CREATED_BY => [
                'required',
                Rule::exists(UserDefinition::TABLE_NAME, UserDefinition::ID)
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
