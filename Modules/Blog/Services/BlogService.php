<?php

namespace Modules\Blog\Services;

use App\Repositories\Contracts\RepositoryInterface;
use App\Services\BaseService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Modules\Blog\Repositories\BlogRepository;
use App\Services\ServiceHelper;
use Modules\Blog\Entities\Blog;
use Modules\User\Entities\User;
use Modules\Blog\Entities\BlogDefinition;
use Modules\User\Entities\UserDefinition;

/** @property BlogRepository $primaryRepository */
class BlogService extends BaseService
{
    public function __construct(BlogRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @param string|null $searchText
     * @param array $orderBy
     * @param bool $includeDefused
     * @return Builder[]|Collection
     */
    public function query(?string $searchText, $orderBy = array(), bool $includeDefused = false)
    {
        return $this->primaryRepository->query($searchText, $orderBy, $includeDefused);
    }

    /**
     * @param string|null $searchText
     * @param int $pageSize
     * @param array $orderBy
     * @param bool $includeDefused
     * @return LengthAwarePaginator
     */
    public function queryPaginated(?string $searchText, $pageSize = 15, $orderBy = array(), bool $includeDefused = false): LengthAwarePaginator
    {
        return $this->primaryRepository->queryPaginated($searchText, $pageSize, $orderBy, $includeDefused);
    }

    /**
     * @param User $createdBy
     * @param string $title
     * @param string|null $description
     * @param int $views
     * @return Blog
     */
    public function addBlog(User $createdBy, string $title, ?string $description, int $views): Blog
    {
        return $this->create([
            BlogDefinition::CREATED_BY => $createdBy[UserDefinition::ID],
            BlogDefinition::TITLE => $title,
            BlogDefinition::DESCRIPTION => $description,
            BlogDefinition::VIEWS => $views,
        ]);
    }

    /**
     * @param Blog $blog
     * @param User $createdBy
     * @param string $title
     * @param string|null $description
     * @param int $views
     * @return Blog
     */
    public function editBlog(Blog $blog, User $createdBy, string $title, ?string $description, int $views): Blog
    {
        $blog->fill([
            BlogDefinition::CREATED_BY => $createdBy[UserDefinition::ID],
            BlogDefinition::TITLE => $title,
            BlogDefinition::DESCRIPTION => $description,
            BlogDefinition::VIEWS => $views,
        ]);
        $this->save($blog);
        return $blog;
    }

    /**
     * @param Blog $blog
     * @throws Exception
     */
    public function deleteBlog(Blog $blog)
    {
        $this->softDelete($blog[BlogDefinition::ID]);
    }
}
