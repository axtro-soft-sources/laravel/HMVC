<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Blog\Entities\BlogDefinition;
use Modules\User\Entities\UserDefinition;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(BlogDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(BlogDefinition::TITLE);
            $table->longText(BlogDefinition::DESCRIPTION);
            $table->integer(BlogDefinition::VIEWS);
            $table->unsignedInteger(BlogDefinition::CREATED_BY);

            $table->foreign(BlogDefinition::CREATED_BY)
                ->references(UserDefinition::ID)
                ->on(UserDefinition::TABLE_NAME)
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(BlogDefinition::TABLE_NAME);
    }
};
