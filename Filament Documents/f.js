function openNav() {
    document.getElementById("mySidenav").style.right = "0px"
}

function closeNav() {
    document.getElementById("mySidenav").style.right = "-260px"
}

tailwind.config = {
    theme: {
        extend: {
            flex: {2: "2 2 0%", 3: "3 3 0%"},
            colors: {clifford: "#da373d"},
            screens: {
                xxsmall: {min: "0px", max: "375px"},
                xsmall: {min: "376px", max: "520px"},
                small: {min: "521px", max: "650px"},
                meduim: {min: "651px", max: "900px"},
                larg: {min: "901px", max: "1300px"},
                xlarg: {min: "1301px", max: "1440px"},
                xxlarg: {min: "1441px", max: "2560px"}
            }
        }
    }
}, $("#buttonopen").click((function () {
    $("#buttonopen").css("display", "none"), $("#buttonclose").css("display", "block")
})), $("#buttonclose").click((function () {
    $("#buttonclose").css("display", "none"), $("#buttonopen").css("display", "block")
})), $(".openSubMenu1").click((function () {
    $(".subMenu1").slideDown(200), $(".subMenu1").css("display", "block"), $(".openSubMenu1").css("display", "none"), $(".closeSubMenu1").css("display", "block")
})), $(".closeSubMenu1").click((function () {
    $(".subMenu1").slideUp(200), $(".subMenu1").css("display", "none"), $(".closeSubMenu1").css("display", "none"), $(".openSubMenu1").css("display", "block")
})), $(".openSubMenu2").click((function () {
    $(".subMenu2").slideDown(200), $(".subMenu2").css("display", "block"), $(".openSubMenu2").css("display", "none"), $(".closeSubMenu2").css("display", "block")
})), $(".closeSubMenu2").click((function () {
    $(".subMenu2").slideUp(200), $(".subMenu2").css("display", "none"), $(".closeSubMenu2").css("display", "none"), $(".openSubMenu2").css("display", "block")
})), $(".show-slider").click((function () {
    $(".slider-image").css("display", "block")
})), $(".close-show-slider").click((function () {
    $(".slider-image").css("display", "none")
})), $(document).ready((function () {
    $(document).scroll((function () {
        var scroll, topDist;
        $(this).scrollTop() > $("#container").position().top ? $("#container").css({
            position: "fixed",
            top: "0"
        }) : (window.screen.width >= 901 && window.screen.width, $("#container").css({
            position: "absolute",
            top: "100%"
        }))
    }))
})), $(document).ready((function () {
    $(document).scroll((function () {
        var scroll2, topDist2;
        $(this).scrollTop() > $("#container2").position().top ? $("#container2").css({
            position: "fixed",
            top: "0"
        }) : window.screen.width >= 0 && $("#container2").css({position: "absolute", top: "100%"})
    }))
})), $(document).ready((function () {
    $(document).scroll((function () {
        var scroll3, topDist3;
        $(this).scrollTop() > $("#container3").position().top ? $("#container3").css({
            position: "fixed",
            top: "0"
        }) : window.screen.width >= 0 && $("#container3").css({position: "static", top: "auto"})
    }))
}));
var swiper = new Swiper(".mySwiperHome", {
    rewind: !0,
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"}
}), swiper = new Swiper(".mySwiper22", {
    slidesPerView: 3,
    spaceBetween: -20,
    slidesPerGroup: 1,
    loop: !0,
    autoplay: {delay: 2e3},
    loopFillGroupWithBlank: !0,
    pagination: {el: ".swiper-pagination", clickable: !0},
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    breakpoints: {
        0: {slidesPerView: 1, spaceBetween: 30, slidesPerGroup: 1},
        376: {slidesPerView: 1, spaceBetween: 30, slidesPerGroup: 1},
        521: {slidesPerView: 2, spaceBetween: 40, slidesPerGroup: 1},
        651: {slidesPerView: 2, spaceBetween: 30, slidesPerGroup: 1},
        901: {slidesPerView: 3, spaceBetween: 30, slidesPerGroup: 1}
    }
}), swiper = new Swiper(".mySwiper3", {
    slidesPerView: 3,
    spaceBetween: -50,
    slidesPerGroup: 1,
    loop: !0,
    autoplay: {delay: 3e3},
    loopFillGroupWithBlank: !0,
    pagination: {el: ".swiper-pagination", clickable: !0},
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    breakpoints: {
        0: {slidesPerView: 1, spaceBetween: 40, slidesPerGroup: 1},
        376: {slidesPerView: 1, spaceBetween: 40, slidesPerGroup: 1},
        521: {slidesPerView: 2, spaceBetween: 40, slidesPerGroup: 1},
        651: {slidesPerView: 2, spaceBetween: 40, slidesPerGroup: 1},
        901: {slidesPerView: 3, spaceBetween: 40, slidesPerGroup: 1}
    }
}), swiper = new Swiper(".mySwiper", {
    loop: !0,
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: !0,
    autoplay: {delay: 2e3},
    watchSlidesProgress: !0
}), swiper2 = new Swiper(".mySwiper2", {
    loop: !0,
    spaceBetween: 10,
    autoplay: {delay: 2e3},
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    thumbs: {swiper: swiper}
});
