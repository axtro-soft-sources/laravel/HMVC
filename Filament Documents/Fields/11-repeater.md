## Repeater
The repeater component allows you to output a JSON array of repeated form components.
```
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;

Repeater::make('members')
    ->schema([
        TextInput::make('name')->required(),
        Select::make('role')
            ->options([
                'member' => 'Member',
                'administrator' => 'Administrator',
                'owner' => 'Owner',
            ])
            ->required(),
    ])
    ->columns(2)
    ->defaultItems(3)
    ->createItemButtonLabel('Add member')
    ->disableItemCreation()
    ->disableItemDeletion()
    ->disableItemMovement()
    ->minItems(1)
    ->maxItems(10)
    ->collapsible()
    ->collapsed()
    
```
We recommend that you store repeater data with a JSON column in your database. Additionally, if you're using Eloquent, make sure that column has an array cast.

As evident in the above example, the component schema can be defined within the schema() method of the component:
```
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\TextInput;
 
Repeater::make('members')
    ->schema([
        TextInput::make('name')->required(),
        // ...
    ])
```
You may allow repeater items to be duplicated using the cloneable() method:
```
Repeater::make('qualifications')
    ->schema([
        // ...
    ])
    ->cloneable()
```
### Populating automatically from a relationship
You may employ the relationship() method of the repeater to configure a relationship to automatically retrieve and save repeater items:
```
Repeater::make('qualifications')
    ->relationship()
    ->schema([
        // ...
    ])
    ->orderable('order_column')
```
You may organize repeater items into columns by using the grid() method:

This method accepts the same options as the columns() method of the grid. This allows you to responsively customize the number of grid columns at various breakpoints.
```
Repeater::make('members')
    ->schema([
        // ...
    ])
    ->grid(2)
```
You may add a label for repeater items using the itemLabel() method:
```
Repeater::make('members')
    ->schema([
        TextInput::make('name')
            ->lazy(),
    ])
    ->itemLabel(fn (array $state): ?string => $state['name'] ?? null),
```
## Access Parent Field Values
Using $get() to access parent field values
All form components are able to use $get() and $set() to access another field's value. However, you might experience unexpected behaviour when using this inside the repeater's schema.

This is because `$get()` and `$set()`, by default, are scoped to the current repeater item. This means that you are able to interact with another field inside that repeater item easily without knowing which repeater item the current form component belongs to.

The consequence of this, is that you may be confused when you are unable to interact with a field outside the repeater. We use ../ syntax to solve this problem - `$get('../../parent_field_name')`.

Consider your form has this data structure:
```
[
    'client_id' => 1,
 
    'repeater' => [
        'item1' => [
            'service_id' => 2,
        ],
    ],
]
```
You are trying to retrieve the value of client_id from inside the repeater item.

`$get()` is relative to the current repeater item, so `$get('client_id')` is looking for `$get('repeater.item1.client_id')`.

You can use ../ to go up a level in the data structure, so `$get('../client_id')` is `$get('repeater.client_id')` and `$get('../../client_id')` is `$get('client_id')`.

