## Radio
```
use Filament\Forms\Components\Radio;
 
Radio::make('status')
    ->options([
        'draft' => 'Draft',
        'scheduled' => 'Scheduled',
        'published' => 'Published'
    ])
```
You can optionally provide descriptions to each option using the descriptions() method:
```
Radio::make('status')
    ->options([
        'draft' => 'Draft',
        'scheduled' => 'Scheduled',
        'published' => 'Published'
    ])
    ->descriptions([
        'draft' => 'Is not visible.',
        'scheduled' => 'Will be visible.',
        'published' => 'Is visible.'
    ])
```
If you want a simple boolean radio button group, with "Yes" and "No" options, you can use the boolean() method:
```
Radio::make('feedback')
    ->label('Do you like this post?')
    ->boolean()
    ->inline()
```
