## Select
The select component allows you to select from a list of predefined options:
```
use Filament\Forms\Components\Select;

Select::make('status')
    ->options([
    'draft' => 'Draft',
    'reviewing' => 'Reviewing',
    'published' => 'Published',
])
```
You may enable a search input to allow easier access to many options, using the searchable() method:
```
Select::make('authorId')
    ->label('Author')
    ->options(User::all()->pluck('name', 'id'))
    ->searchable()
```
If you have lots of options and want to populate them based on a database search or other external data source, you can use the getSearchResultsUsing() and getOptionLabelUsing() methods instead of options().

The getSearchResultsUsing() method accepts a callback that returns search results in $key => $value format.

The getOptionLabelUsing() method accepts a callback that transforms the selected option $value into a label.
```
Select::make('authorId')
    ->searchable()
    ->getSearchResultsUsing(fn (string $search) => User::where('name', 'like', "%{$search}%")->limit(50)->pluck('name', 'id'))
    ->getOptionLabelUsing(fn ($value): ?string => User::find($value)?->name),    
```
You can prevent the placeholder from being selected using the disablePlaceholderSelection() method:
```
Select::make('status')
    ->options([
        'draft' => 'Draft',
        'reviewing' => 'Reviewing',
        'published' => 'Published',
    ])
    ->default('draft')
    ->disablePlaceholderSelection()
```
## Multi-select
The multiple() method on the Select component allows you to select multiple values from the list of options:
```
Select::make('technologies')
    ->multiple()
    ->options([
    'tailwind' => 'Tailwind CSS',
    'alpine' => 'Alpine.js',
    'laravel' => 'Laravel',
    'livewire' => 'Laravel Livewire',
])
```
These options are returned in JSON format. If you're saving them using Eloquent, you should be sure to add an array cast to the model property:
```
class App extends Model
{
    protected $casts = [
        'technologies' => 'array',
    ];
 
    // ...
}
```
Populating automatically from a relationship:
```
Select::make('authorId')
    ->multiple()
    ->relationship('author', 'name')
```
If you'd like to populate the options from the database when the page is loaded, instead of when the user searches, you can use the preload() method:
```
Select::make('authorId')
    ->relationship('author', 'name')
    ->preload()
```
You may customize the database query that retrieves options using the third parameter of the relationship() method:
```
Select::make('authorId')
    ->relationship('author', 'name', fn (Builder $query) => $query->withTrashed())
```
If you'd like to customize the label of each option, maybe to be more descriptive, or to concatenate a first and last name, you should use a virtual column in your database migration:
```
$table->string('full_name')->virtualAs('concat(first_name, \' \', last_name)');
...
Select::make('authorId')
    ->relationship('author', 'full_name')
```
Alternatively, you can use the getOptionLabelFromRecordUsing() method to transform the selected option's Eloquent model into a label. But please note, this is much less performant than using a virtual column:
```
Select::make('authorId')
    ->relationship('author', 'first_name')
    ->getOptionLabelFromRecordUsing(fn (Model $record) => "{$record->first_name} {$record->last_name}")
```
Handling MorphTo relationships
```
use Filament\Forms\Components\MorphToSelect;

MorphToSelect::make('commentable')
    ->types([
        MorphToSelect\Type::make(Product::class)->titleColumnName('name'),
        MorphToSelect\Type::make(Post::class)->titleColumnName('title'),
    ])
    
MorphToSelect::make('commentable')
    ->types([
        MorphToSelect\Type::make(Product::class)
            ->getOptionLabelFromRecordUsing(fn (Product $record): string => "{$record->name} - {$record->slug}"),
        MorphToSelect\Type::make(Post::class)->titleColumnName('title'),
    ])    
```
You may customize the database query that retrieves options using the modifyOptionsQueryUsing() method:
```
use Filament\Forms\Components\MorphToSelect;
use Illuminate\Database\Eloquent\Builder;

MorphToSelect::make('commentable')
    ->types([
        MorphToSelect\Type::make(Product::class)
            ->titleColumnName('name')
            ->modifyOptionsQueryUsing(fn (Builder $query) => $query->whereBelongsTo($this->team)),
        MorphToSelect\Type::make(Post::class)
            ->titleColumnName('title')
            ->modifyOptionsQueryUsing(fn (Builder $query) => $query->whereBelongsTo($this->team)),
    ])
```
#### Creating new records
You may define a custom form that can be used to create a new record and attach it to the BelongsTo relationship:
```
use Filament\Forms\Components\Select;
use Illuminate\Database\Eloquent\Model;
 
Select::make('authorId')
    ->relationship('author', 'name')
    ->createOptionForm([
        Forms\Components\TextInput::make('name')
            ->required(),
        Forms\Components\TextInput::make('email')
            ->required()
            ->email(),
    ]),
```
