## input
The checkbox component, similar to a toggle, allows you to interact a boolean value.
```
use Filament\Forms\Components\Checkbox;

Checkbox::make('is_admin')
```

When the checkbox is stacked, its label is above it:
```
Checkbox::make('is_admin')->inline(false)
```
If you're saving the boolean value using Eloquent, you should be sure to add a boolean cast to the model property:
```
class User extends Model
{
    protected $casts = [
        'is_admin' => 'boolean',
    ];
 
    // ...
}
```
## Toggle
```
use Filament\Forms\Components\Toggle;
 
Toggle::make('is_admin')
Toggle::make('is_admin')->inline(false)

Toggle::make('is_admin')
    ->onIcon('heroicon-s-lightning-bolt')
    ->offIcon('heroicon-s-user')
   
Toggle::make('is_admin')
    ->onColor('success')
    ->offColor('danger')
```
## Checkbox list
```
use Filament\Forms\Components\CheckboxList;

class App extends Model
{
    protected $casts = [
        'technologies' => 'array',
    ];
 
    // ...
}
...
CheckboxList::make('technologies')
    ->options([
        'tailwind' => 'Tailwind CSS',
        'alpine' => 'Alpine.js',
        'laravel' => 'Laravel',
        'livewire' => 'Laravel Livewire',
    ])
```
You may also allow users to toggle all checkboxes at once using the bulkToggleable() method:
```
CheckboxList::make('technologies')
    ->options([
        'tailwind' => 'Tailwind CSS',
        'alpine' => 'Alpine.js',
        'laravel' => 'Laravel',
        'livewire' => 'Laravel Livewire',
    ])
    ->bulkToggleable()
```
Populating automatically from a relationship
```
CheckboxList::make('technologies')
    ->relationship('technologies', 'name')
```
You may customize the database query that retrieves options using the third parameter of the relationship() method:
```
use Illuminate\Database\Eloquent\Builder;
 
CheckboxList::make('technologies')
    ->relationship('technologies', 'name', fn (Builder $query) => $query->withTrashed())
```
If you'd like to customize the label of each option, maybe to be more descriptive, or to concatenate a first and last name, you should use a virtual column in your database migration:
```
$table->string('full_name')->virtualAs('concat(first_name, \' \', last_name)');
...
CheckboxList::make('participants')
    ->relationship('participants', 'full_name')
```
Alternatively, you can use the getOptionLabelFromRecordUsing() method to transform the selected option's Eloquent model into a label. But please note, this is much less performant than using a virtual column:
```
use Filament\Forms\Components\CheckboxList;
use Illuminate\Database\Eloquent\Model;
 
CheckboxList::make('participants')
    ->relationship('participants', 'first_name')
    ->getOptionLabelFromRecordUsing(fn (Model $record) => "{$record->first_name} {$record->last_name}")
```
