## Key-Value
The key-value field allows you to interact with one-dimensional JSON object:
```
use Filament\Forms\Components\KeyValue;

KeyValue::make('meta')
    ->keyLabel('Property name')
    ->valueLabel('Property value')
    ->keyPlaceholder('Property name')
    ->valuePlaceholder('Property value')    
    ->disableAddingRows()
    ->disableDeletingRows()
    ->disableEditingKeys()
    ->reorderable()
        
```
You may customize the labels for the key and value fields using the keyLabel() and valueLabel() methods:
```
KeyValue::make('meta')

```
