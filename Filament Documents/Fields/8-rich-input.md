## Rich Editor
```
use Filament\Forms\Components\RichEditor;
 
RichEditor::make('content')
    ->toolbarButtons([
        'attachFiles',
        'blockquote',
        'bold',
        'bulletList',
        'codeBlock',
        'h2',
        'h3',
        'italic',
        'link',
        'orderedList',
        'redo',
        'strike',
        'undo',
    ])
    ->disableToolbarButtons([
        'attachFiles',
        'codeBlock',
    ])
    ->enableToolbarButtons([
        'bold',
        'bulletList',
        'italic',
        'strike',
    ])    
```
You may customize how images are uploaded using configuration methods:
```
RichEditor::make('content')
    ->fileAttachmentsDisk('s3')
    ->fileAttachmentsDirectory('attachments')
    ->fileAttachmentsVisibility('private')
```
