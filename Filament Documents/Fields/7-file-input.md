## File upload
```
use Filament\Forms\Components\FileUpload;

FileUpload::make('attachment')
```
To change the disk and directory that files are saved in, and their visibility, use the disk(), directory() and visibility methods:
```
FileUpload::make('attachment')
    ->disk('s3')
    ->directory('form-attachments')
    ->visibility('private')
```
By default, a random file name will be generated for newly-uploaded files. To instead preserve the original filenames of the uploaded files, use the preserveFilenames() method:
```
FileUpload::make('attachment')->preserveFilenames()
```
You may completely customize how file names are generated using the getUploadedFileNameForStorageUsing() method, and returning a string from the callback:
```
FileUpload::make('attachment')
    ->getUploadedFileNameForStorageUsing(function (TemporaryUploadedFile $file): string {
        return (string) str($file->getClientOriginalName())->prepend('custom-prefix-');
    })
```
You can keep the randomly generated file names, while still storing the original file name, using the storeFileNamesIn() method:
```
FileUpload::make('attachments')
    ->multiple()
    ->storeFileNamesIn('attachment_file_names')
```
attachment_file_names will now store the original file name/s of your uploaded files.

You may restrict the types of files that may be uploaded using the acceptedFileTypes() method, and passing an array of MIME types. You may also use the image() method as shorthand to allow all image MIME types.
```
FileUpload::make('document')->acceptedFileTypes(['application/pdf'])
FileUpload::make('image')->image()
```
You may also restrict the size of uploaded files, in kilobytes:
```
FileUpload::make('attachment')
    ->minSize(512)
    ->maxSize(1024)
```
Filepond allows you to crop and resize images before they are uploaded. You can customize this behaviour using the imageCropAspectRatio(), imageResizeTargetHeight() and imageResizeTargetWidth() methods.
```
FileUpload::make('image')
    ->image()
    ->imageCropAspectRatio('16:9')
    ->imageResizeTargetWidth('1920')
    ->imageResizeTargetHeight('1080')
```
You may also alter the general appearance of the Filepond component. Available options for these methods are available on the Filepond website.
```
FileUpload::make('attachment')
    ->imagePreviewHeight('250')
    ->loadingIndicatorPosition('left')
    ->panelAspectRatio('2:1')
    ->panelLayout('integrated')
    ->removeUploadedFileButtonPosition('right')
    ->uploadButtonPosition('left')
    ->uploadProgressIndicatorPosition('left')
```
You may also upload multiple files. This stores URLs in JSON:
```
use Illuminate\Database\Eloquent\Model;
 
class Message extends Model
{
    protected $casts = [
        'attachments' => 'array',
    ];
 
    // ...
}
...
use Filament\Forms\Components\FileUpload;
 
FileUpload::make('attachments')
    ->multiple()
    ->minFiles(2)
    ->maxFiles(5)
    ->enableReordering()
    ->enableOpen()
    ->enableDownload()
```
