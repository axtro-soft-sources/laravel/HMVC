## Color Picker
The color picker component allows you to pick a color in a range of formats.

By default, the component uses HEX format:
```
use Filament\Forms\Components\ColorPicker;
 
ColorPicker::make('color')
ColorPicker::make('hsl_color')->hsl()
ColorPicker::make('rgb_color')->rgb()
ColorPicker::make('rgba_color')->rgba()
```
