## Markdown Editor
The markdown editor allows you to edit and preview markdown content, as well as upload images using drag and drop.
```
use Filament\Forms\Components\MarkdownEditor;
 
MarkdownEditor::make('content')
    ->toolbarButtons([
        'attachFiles',
        'bold',
        'bulletList',
        'codeBlock',
        'edit',
        'italic',
        'link',
        'orderedList',
        'preview',
        'strike',
    ])
    ->disableToolbarButtons([
        'attachFiles',
        'codeBlock',
    ])
    ->enableToolbarButtons([
        'bold',
        'bulletList',
        'edit',
        'italic',
        'preview',
        'strike',
    ])    
```
You may customize how images are uploaded using configuration methods:
```
MarkdownEditor::make('content')
    ->fileAttachmentsDisk('s3')
    ->fileAttachmentsDirectory('attachments')
    ->fileAttachmentsVisibility('private')
```
