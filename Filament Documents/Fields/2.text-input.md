## Text input
#### examples:
```
use Filament\Forms\Components\TextInput;
TextInput::make('name')
    ->minLength(2)
    ->maxLength(255)
    ->length(8)
    ->email()
    ->numeric()
    ->password()
    ->tel()
    ->url()
    ->type('color')
    
TextInput::make('number')
    ->numeric()
    ->minValue(1)
    ->maxValue(100)

TextInput::make('password')
    ->password()
    ->autocomplete('new-password')
    
TextInput::make('password')
    ->password()
    ->disableAutocomplete()

TextInput::make('phone')
    ->tel()
    ->telRegex('/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/')

TextInput::configureUsing(function (TextInput $component): void {
    $component->telRegex('/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/');
});

TextInput::make('domain')
    ->url()
    ->prefix('https://')
    ->suffix('.com')
// this code will return  label-input-label

TextInput::make('domain')
    ->url()
    ->prefixIcon('heroicon-s-external-link')
    ->suffixIcon('heroicon-s-external-link')
    
TextInput::make('domain')
    ->suffixAction(
        Action::make('visit')
            ->icon('heroicon-s-external-link')
            ->url(
                fn (?string $state): ?string => filled($state) ? "https://{$state}" : null,
                shouldOpenInNewTab: true,
            ),
    )                        
```
#### masking
```
TextInput::make('name')
    ->mask(fn (TextInput\Mask $mask) => $mask->pattern('+{7}(000)000-00-00'))

TextInput::make('number')
    ->numeric()
    ->mask(fn (TextInput\Mask $mask) => $mask
        ->numeric()
        ->decimalPlaces(2) // Set the number of digits after the decimal point.
        ->decimalSeparator(',') // Add a separator for decimal numbers.
        ->integer() // Disallow decimal numbers.
        ->mapToDecimalSeparator([',']) // Map additional characters to the decimal separator.
        ->minValue(1) // Set the minimum value that the number can be.
        ->maxValue(100) // Set the maximum value that the number can be.
        ->normalizeZeros() // Append or remove zeros at the end of the number.
        ->padFractionalZeros() // Pad zeros at the end of the number to always maintain the maximum number of decimal places.
        ->thousandsSeparator(','), // Add a separator for thousands.
    )

TextInput::make('code')->mask(fn (TextInput\Mask $mask) => $mask->enum(['F1', 'G2', 'H3']))

TextInput::make('code')->mask(fn (TextInput\Mask $mask) => $mask
    ->range()
    ->from(1) // Set the lower limit.
    ->to(100) // Set the upper limit.
    ->maxValue(100), // Pad zeros at the start of smaller numbers.
)    

// In addition to simple pattens, you may also define multiple pattern blocks:
TextInput::make('cost')->mask(fn (TextInput\Mask $mask) => $mask
    ->patternBlocks([
        'money' => fn (Mask $mask) => $mask
            ->numeric()
            ->thousandsSeparator(',')
            ->decimalSeparator('.'),
    ])
    ->pattern('$money'),
)
TextInput::make('cost')->mask(fn (TextInput\Mask $mask) => $mask->money(prefix: '$', thousandsSeparator: ',', decimalPlaces: 2))

// You can also control whether the number is signed or not. While the default is to allow both negative and positive numbers, isSigned: false allows only positive numbers:
TextInput::make('cost')->mask(fn (TextInput\Mask $mask) => $mask->money(prefix: '$', thousandsSeparator: ',', decimalPlaces: 2, isSigned: false))


```
