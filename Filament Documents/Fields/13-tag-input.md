## Tag Input
The tags input component allows you to interact with a list of tags.

By default, tags are stored in JSON:
```
use Filament\Forms\Components\TagsInput;
 
TagsInput::make('tags')
```
If you're saving the JSON tags using Eloquent, you should be sure to add an array cast to the model property:
```
use Illuminate\Database\Eloquent\Model;
 
class Post extends Model
{
    protected $casts = [
        'tags' => 'array',
    ];
 
    // ...
}
```
You may allow the tags to be stored in a separated string, instead of JSON. To set this up, pass the separating character to the separator() method:
```
use Filament\Forms\Components\TagsInput;
 
TagsInput::make('tags')->separator(',')
```
Tags inputs may have autocomplete suggestions. To enable this, pass an array of suggestions to the suggestions() method:
```
use Filament\Forms\Components\TagsInput;
 
TagsInput::make('tags')
    ->suggestions([
        'tailwindcss',
        'alpinejs',
        'laravel',
        'livewire',
    ])
```
