## Textarea
The textarea allows you to interact with a multi-line string:
```
use Filament\Forms\Components\Textarea;
 
Textarea::make('description')
    ->rows(10)
    ->cols(20)
    ->minLength(50)
    ->maxLength(500)
```


