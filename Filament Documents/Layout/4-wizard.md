## Wizard
Similar to tabs, you may want to use a multistep form wizard to reduce the number of components that are visible at once. These are especially useful if your form has a definite chronological order, in which you want each step to be validated as the user progresses.
```
use Filament\Forms\Components\Wizard;
 
Wizard::make([
    Wizard\Step::make('Order')
        ->schema([
            // ...
        ]),
    Wizard\Step::make('Delivery')
        ->schema([
            // ...
        ]),
    Wizard\Step::make('Billing')
        ->schema([
            // ...
        ]),
])
```
Each step has a mandatory label. You may optionally also add a description for extra detail:
```
Wizard\Step::make('Order')
    ->description('Review your basket')
    ->schema([
        // ...
    ]),
```
Steps may also have an icon, which can be the name of any Blade icon component:
```
Wizard\Step::make('Order')
    ->icon('heroicon-o-shopping-bag')
    ->schema([
        // ...
    ]),
```
You may use the submitAction() method to render submit button HTML or a view at the end of the wizard, on the last step. This provides a clearer UX than displaying a submit button below the wizard at all times:
```
use Filament\Forms\Components\Wizard;
use Illuminate\Support\HtmlString;

...

Wizard::make([
    // ...
])->submitAction(view('order-form.submit-button'))
 
Wizard::make([
    // ...
])->submitAction(new HtmlString('<button type="submit">Submit</button>'))
```
You may use the startOnStep() method to load a specific step in the wizard:
```
use Filament\Forms\Components\Wizard;
 
Wizard::make([
    // ...
])->startOnStep(2)
```
If you'd like to allow free navigation, so all steps are skippable, use the skippable() method:
```
Wizard::make([
    // ...
])->skippable()
```
