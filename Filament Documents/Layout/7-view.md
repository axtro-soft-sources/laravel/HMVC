## View
Aside from building custom layout components, you may create "view" components which allow you to create custom layouts without extra PHP classes.
```
use Filament\Forms\Components\View;
 
View::make('filament.forms.components.wizard')
```
Inside your view, you may render the component's schema() using the $getChildComponentContainer() closure:
```
<div>
    {{ $getChildComponentContainer() }}
</div>
```
