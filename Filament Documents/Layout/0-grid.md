## Grid
```
use Filament\Forms\Components\Grid;
 
Grid::make()
    ->schema([
        // ...
    ])
    
Card::make()->columns(2)
```
### Controlling field column span
You may specify the number of columns that any component may span in the parent grid:
```
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\TextInput;
 
Grid::make(3)
    ->schema([
        TextInput::make('name')
            ->columnSpan(2),
        // ...
    ])
```
You may use columnSpan('full') to ensure that a column spans the full width of the parent grid, however many columns it has:
```
Grid::make(3)
    ->schema([
        TextInput::make('name')
            ->columnSpan('full'),
        // ...
    ])
```
Instead, you can even define how many columns a component may consume at any breakpoint:
```
Grid::make([
    'default' => 1,
    'sm' => 3,
    'xl' => 6,
    '2xl' => 8,
])
    ->schema([
        TextInput::make('name')
            ->columnSpan([
                'sm' => 2,
                'xl' => 3,
                '2xl' => 4,
            ]),
        // ...
    ])
```
### Setting an ID
You may define an ID for the component using the id() method:
```
use Filament\Forms\Components\Card;
 
Card::make()->id('main-card')
```
### Custom attributes
The HTML of components can be customized even further, by passing an array of extraAttributes():
```
use Filament\Forms\Components\Card;
 
Card::make()->extraAttributes(['class' => 'bg-gray-50'])
```
### Global settings
If you wish to change the default behaviour of a component globally, then you can call the static configureUsing() method inside a service provider's boot() method, to which you pass a Closure to modify the component using. For example, if you wish to make all card components have 2 columns by default, you can do it like so:
```
use Filament\Forms\Components\Card;
 
Card::configureUsing(function (Card $card): void {
    $card->columns(2);
});
```
Of course, you are still able to overwrite this on each field individually:
```
use Filament\Forms\Components\Card;
 
Card::make()->columns(1)
```
### Saving data to relationships
You may load and save the contents of a layout component to a HasOne, BelongsTo or MorphOne Eloquent relationship, using the relationship() method:
```
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
 
Fieldset::make('Metadata')
    ->relationship('metadata')
    ->schema([
        TextInput::make('title'),
        Textarea::make('description'),
        FileUpload::make('image'),
    ])
```
Generally, form fields are stacked on top of each other in one column. To change this, you may use a grid component:
```
use Filament\Forms\Components\Grid;
 
Grid::make()
    ->schema([
        // ...
    ])
    
Grid::make(3) // 3 coloumns
    ->schema([
        // ...
    ])    
    
Grid::make([
    'default' => 1,
    'sm' => 2,
    'md' => 3,
    'lg' => 4,
    'xl' => 6,
    '2xl' => 8,
])
    ->schema([
        // ...
    ])
    
Grid::make([
    'sm' => 2,
    'xl' => 6,
])
    ->schema([
        // ...
    ])    
```
