## Tabs
Some forms can be long and complex. You may want to use tabs to reduce the number of components that are visible at once:
```
use Filament\Forms\Components\Tabs;
 
Tabs::make('Heading')
    ->tabs([
        Tabs\Tab::make('Label 1')
            ->schema([
                // ...
            ]),
        Tabs\Tab::make('Label 2')
            ->schema([
                // ...
            ]),
        Tabs\Tab::make('Label 3')
            ->schema([
                // ...
            ]),
    ])
```
The first tab will be open by default. You can change the default open tab using the activeTab() method:
```
Tabs::make('Heading')
    ->tabs([
        Tabs\Tab::make('Label 1')
            ->schema([
                // ...
            ]),
        Tabs\Tab::make('Label 2')
            ->schema([
                // ...
            ]),
        Tabs\Tab::make('Label 3')
            ->schema([
                // ...
            ]),
    ])
    ->activeTab(2)
```
Tabs may have an icon and badge, which you can set using the icon() and badge() methods:
```
Tabs::make('Heading')
    ->tabs([
        Tabs\Tab::make('Notifications')
            ->icon('heroicon-o-bell') 
            ->badge('39') 
            ->schema([
                // ...
            ]),
        // ...
    ])
```
