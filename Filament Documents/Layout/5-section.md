## Section
You may want to separate your fields into sections, each with a heading and description. To do this, you can use a section component:
```
use Filament\Forms\Components\Section;
 
Section::make('Heading')
    ->description('Description')
    ->schema([
        // ...
    ])
```
You may use the columns() method to easily create a grid within the section:
```
Section::make('Heading')
    ->schema([
        // ...
    ])
    ->columns(2)
```
You may use the aside() to align heading & description on the left, and the form components inside a card on the right:
```
Section::make('Heading')
    ->description('Description')
    ->aside()
    ->schema([
        // ...
    ])
```
Sections may be collapsible() to optionally hide content in long forms:
```
Section::make('Heading')
    ->schema([
        // ...
    ])
    ->collapsible()
    ->collapsed()
```
When nesting sections, you can use a more compact styling:
```
Section::make('Heading')
    ->schema([
        // ...
    ])
    ->compact()
```
