## Card
The card component may be used to render the form components inside a card:
```
use Filament\Forms\Components\Card;
 
Card::make()
    ->schema([
        // ...
    ])
```
You may use the columns() method to easily create a grid within the card:
```
Card::make()
    ->schema([
        // ...
    ])
    ->columns(2)
```
