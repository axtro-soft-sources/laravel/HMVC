## Fieldset
You may want to group fields into a Fieldset. Each fieldset has a label, a border, and a two-column grid by default:
```
use Filament\Forms\Components\Fieldset;
 
Fieldset::make('Label')
    ->schema([
        // ...
    ])
    
Fieldset::make('Label')
    ->schema([
        // ...
    ])
    ->columns(3)    
```
