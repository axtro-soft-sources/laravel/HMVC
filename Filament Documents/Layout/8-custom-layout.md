## Building custom layout components
You may create your own custom component classes and views, which you can reuse across your project, and even release as a plugin to the community.

To create a custom column class and view, you may use the following command:

```
php artisan make:form-layout Wizard
```
This will create the following layout component class:
```
use Filament\Forms\Components\Component;
 
class Wizard extends Component
{
    protected string $view = 'filament.forms.components.wizard';
 
    public static function make(): static
    {
        return new static();
    }
}
```
Inside your view, you may render the component's schema() using the $getChildComponentContainer() closure:
```
<div>
    {{ $getChildComponentContainer() }}
</div>
```
