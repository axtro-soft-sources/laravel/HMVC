### General
```
Field::make('name')->string()
Field::make('email')->unique()
Field::make('email')->unique(table: User::class)
Field::make('email')->unique(column: 'email_address')
Field::make('name')->nullable()
Field::make('name')->required()
Field::make('name')->requiredWith('field,another_field')
Field::make('name')->requiredWithAll('field,another_field')
Field::make('name')->requiredWithout('field,another_field')
Field::make('name')->requiredWithoutAll('field,another_field')
Field::make('password')->same('passwordConfirmation')
Field::make('name')->startsWith(['a'])
Field::make('identifer')->uuid()
TextInput::make('slug')->rules(['alpha_dash'])
```
### ignorable unique
Sometimes, you may wish to ignore a given model during unique validation. For example, consider an "update profile" form that includes the user's name, email address, and location. You will probably want to verify that the email address is unique. However, if the user only changes the name field and not the email field, you do not want a validation error to be thrown because the user is already the owner of the email address in question.
```
Field::make('email')->unique(ignorable: $ignoredUser)
```
If you're using the admin panel, you can easily ignore the current record by using ignoreRecord instead:
```
Field::make('email')->unique(ignoreRecord: true)
```
You can further customize the rule by passing a closure to the callback parameter:
```
use Illuminate\Validation\Rules\Unique;
 
Field::make('email')
    ->unique(callback: function (Unique $rule) {
        return $rule->where('is_active', 1);
    })
```
### Prohibited
The field value must be empty. See the Laravel documentation
```
Field::make('name')->prohibited()
```
### Active URL
The field must have a valid A or AAAA record according to the dns_get_record() PHP function. See the Laravel documentation
```
Field::make('name')->activeUrl()
```
### After (date)
The field value must be a value after a given date. See the Laravel documentation
```
Field::make('startDate')
Field::make('endDate')->after('startDate')
Field::make('startDate')->afterOrEqual('tomorrow')
Field::make('endDate')->afterOrEqual('startDate')
Field::make('startDate')->before('first day of next month')
Field::make('startDate')->before('endDate')
Field::make('startDate')->beforeOrEqual('end of this month')
Field::make('startDate')->beforeOrEqual('endDate')
```
### After or equal to (date)
The field value must be a date after or equal to the given date. See the Laravel documentation
### Alpha
The field must be entirely alphabetic characters. See the Laravel documentation
```
Field::make('name')->alpha()
Field::make('name')->alphaDash()
Field::make('name')->alphaNum()
Field::make('name')->doesntStartWith(['admin'])
Field::make('name')->doesntEndWith(['admin'])
Field::make('name')->endsWith(['bot'])

```
### Confirmed
The field must have a matching field of {field}_confirmation. See the Laravel documentation
```
Field::make('password')->confirmed()
Field::make('password_confirmation')
```
### Different
The field value must be different to another. See the Laravel documentation
```
Field::make('backupEmail')->different('email')
```
### Enum
The field must contain a valid enum value. See the Laravel documentation
```
Field::make('status')->enum(MyStatus::class)
```
### Exists
The field value must exist in the database. See the Laravel documentation.
```
Field::make('invitation')->exists()
```
By default, the form's model will be searched, if it is registered. You may specify a custom table name or model to search:
```
use App\Models\Invitation;
 
Field::make('invitation')->exists(table: Invitation::class)
```
By default, the field name will be used as the column to search. You may specify a custom column to search:
```
Field::make('invitation')->exists(column: 'id')
```
You can further customize the rule by passing a closure to the callback parameter:
```
use Illuminate\Validation\Rules\Exists;
 
Field::make('invitation')
    ->exists(callback: function (Exists $rule) {
        return $rule->where('is_active', 1);
    })
```
### Filled
The field must not be empty when it is present. See the Laravel documentation
```
Field::make('name')->filled()
```
### Greater than | Less than
```
Field::make('newNumber')->gt('oldNumber')   // >
Field::make('newNumber')->gte('oldNumber')  // >=
Field::make('newNumber')->lt('oldNumber')   // <
Field::make('newNumber')->lte('oldNumber')  // <=
```
### In
The field must be included in the given list of values. See the Laravel documentation
```
Field::make('status')->in(['pending', 'completed'])
Field::make('status')->notIn(['cancelled', 'rejected'])
```
### Ip Address | Mac Address
```
Field::make('ip_address')->ip()
Field::make('ip_address')->ipv4()
Field::make('ip_address')->ipv6()
Field::make('mac_address')->macAddress()
```
### JSON
```
Field::make('ip_address')->json()
```
### Regex | Not Regex
The field must not match the given regular expression. See the Laravel documentation
```
Field::make('email')->regex('/^.+@.+$/i')
Field::make('email')->notRegex('/^.+$/i')
```
### Custom rules
```
TextInput::make('slug')->rules([new Uppercase()])

TextInput::make('slug')->rules([
    function () {
        return function (string $attribute, $value, Closure $fail) {
            if ($value === 'foo') {
                $fail("The {$attribute} is invalid.");
            }
        };
    },
])
```
### Validation attributes
```
use Filament\Forms\Components\TextInput;
 
TextInput::make('name')->validationAttribute('full name')
```
### Sending validation notifications
```
use Filament\Notifications\Notification;
use Illuminate\Validation\ValidationException;
 
protected function onValidationError(ValidationException $exception): void
{
    Notification::make()
        ->title($exception->getMessage())
        ->danger()
        ->send();
}
```
Alternatively, if you are using admin panel and you want this behaviour on all the pages, add this inside the boot() method of your AppServiceProvider:
```
use Filament\Notifications\Notification;
use Filament\Pages\Page;
use Illuminate\Validation\ValidationException;
 
Page::$reportValidationErrorUsing = function (ValidationException $exception) {
    Notification::make()
        ->title($exception->getMessage())
        ->danger()
        ->send();
};
```
