<?php


namespace App\Entities;


class BaseFields
{
    const ID = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
    const DELETED_AT = "deleted_at";
    const DEACTIVATED_AT = "deactivated_at";
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';
}
