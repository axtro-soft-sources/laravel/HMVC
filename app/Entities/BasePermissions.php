<?php


namespace App\Entities;


abstract class BasePermissions
{
    /**
     * @return array
     * Specify what permissions are migrated to the DB.
     */
    abstract public static function toArray(): array;
}
