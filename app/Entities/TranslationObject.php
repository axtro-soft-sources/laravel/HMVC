<?php

namespace App\Entities;

class TranslationObject
{

    /** @var string */
    public string $en;

    /** @var string  */
    public string $ar;

    /**
     * @param string $en
     * @param string $ar
     */
    public function __construct(string $en, string $ar)
    {
        $this->en = $en;
        $this->ar = $ar;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this;
    }


}
