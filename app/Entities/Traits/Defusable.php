<?php


namespace App\Entities\Traits;


use App\Entities\BaseFields;
use Carbon\Carbon;

/**
 * @method static static|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder withDefused()
 * @method static static|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder withoutDefused()
 * @method static static|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder onlyDefused()
 */
trait Defusable
{
    /**
     * Boot the Defusable trait for a model.
     *
     * @return void
     */
    public static function bootDefusable()
    {
        static::addGlobalScope(new DefusableScope);
    }

    /**
     * Initialize the Defusable trait for an instance.
     *
     * @return void
     */
    public function initializeDefusable()
    {
        $this->dates[] = $this->getDefusableColumnName();
    }

    /**
     * Get the name of the "deactivated at" column.
     *
     * @return string
     */
    public function getDefusableColumnName()
    {
        return BaseFields::DEACTIVATED_AT;
    }

    public function defused()
    {
        return !is_null($this->{$this->getDefusableColumnName()});
    }

    public function defuse()
    {
        $this[BaseFields::DEACTIVATED_AT] = Carbon::now();

        return $this->save();
    }

    public function activate()
    {
        $this[BaseFields::DEACTIVATED_AT] = null;

        return $this->save();
    }
}
