<?php

namespace App\Entities\Traits;

trait Translatable
{
    /**
     * @param array $data
     * @return array
     * @throws \JsonException
     */
    public static function validateTranslatables(array $data): array
    {
        return parent::validateTranslatables($data);
    }
}
