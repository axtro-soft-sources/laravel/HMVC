<?php


namespace App\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class DefusableScope implements Scope
{
    protected $extensions = ['OnlyDefused', 'WithoutDefused', 'WithDefused'];

    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
        //DO NOTHING
        $builder->whereNull($model->getTable() . '.' . $model->getDefusableColumnName());
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param Builder $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    public function addOnlyDefused(Builder $builder)
    {
        $builder->macro('onlyDefused', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNotNull(
                $model->getDefusableColumnName()
            );

            return $builder;
        });
    }

    public function addWithoutDefused(Builder $builder)
    {
        $builder->macro('withoutDefused', function (Builder $builder) {
            return !$builder->onlyDefused();
        });
    }

    public function addWithDefused(Builder $builder)
    {
        $builder->macro('withDefused', function (Builder $builder) {
            return $builder->withoutGlobalScope($this);
        });
    }
}
