<?php

namespace App\Entities;

use Spatie\Translatable\HasTranslations;
use App\Entities\Traits\Translatable;
use GuzzleHttp\Client;

class TranslatableModel extends BaseModel
{
    use HasTranslations, Translatable;

    public const LOCALE_ARABIC = 'ar';
    public const LOCALE_ENGLISH = 'en';

    protected const LANGUAGES = [
        self::LOCALE_ARABIC => [
            "required" => true,
        ],
        self::LOCALE_ENGLISH => [
            "required" => true,
        ],
    ];

    public const DEFAULT_LOCALE = self::LOCALE_ENGLISH;

    public static function isTranslatable(): bool{
        return true;
    }

    /**
     * @throws \JsonException
     */
    public static function languages(){
        return json_decode(json_encode(self::LANGUAGES, JSON_THROW_ON_ERROR), false, 512, JSON_THROW_ON_ERROR);
    }

    public static function strip_tags_array(array $array): array{
        $result = [];
        foreach ($array as $key => $value){
            $result[$key] = strip_tags($value);
        }
        return $result;
    }

    /**
     * @throws \JsonException
     */
    public function getTranslation(string $key, string $locale, bool $useFallbackLocale = true)
    {
        if(isset($_SERVER['HTTP_LOCALE'])) {
            $locale = $_SERVER['HTTP_LOCALE'];
            App()->setLocale($locale);

            $value = $this->getAttributes()[$key];

            if(!json_decode($value)){
                return $value;
            }

            $translations = $this->getTranslations($key);

            $translation = $translations[$locale] ?? '';

            if ($this->hasGetMutator($key)) {
                return $this->mutateAttribute($key, $translation);
            }

            return $translation;
        }else{
            $value = $this->getAttributes()[$key];
            if(!json_decode($value)){
                $result = [];
                foreach ($this->languages() as $loc => $attr){
                    $result[$loc] = $value;
                }
                return $result;
            }
            return $this->getTranslations($key);
        }
    }
    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @throws \JsonException
     */
    public static function translateToAllLocales($word): array
    {
        $result = [];
        $client = new Client(['headers' => [
            'Content-Type' => 'application/json;charset=UTF-8;'
        ]]);
        $key = env('GOOGLE_API_KEY');
        foreach (self::LANGUAGES as $loc => $attr) {
            $response = $client->post('https://translation.googleapis.com/language/translate/v2?key='.$key, ['form_params' => ['q' => $word, 'target' => $loc]]);
            $result[$loc] = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR)['data']['translations'][0]['translatedText'];
        }
        return $result;
    }

    /**
     * @param $attribute
     * @param string $locale
     * @return string
     * @author Waseem Alkhen, Ali Alaa Alden
     */
    public function getTranslateByLangIfExist($attribute, string $locale): string
    {
        return is_string($attribute) ? $attribute : $attribute[$locale];
    }
}
