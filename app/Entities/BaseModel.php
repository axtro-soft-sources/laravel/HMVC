<?php


namespace App\Entities;


use App\User;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Modules\Common\Entities\Traits\Investigatable;
use Modules\User\Entities\UserDefinition;

class BaseModel extends Model
{
    use Cachable;

    protected $hidden = [
        BaseFields::UPDATED_AT,
        BaseFields::CREATED_AT,
        BaseFields::DELETED_AT,
        'laravel_through_key'
    ];

    /**
    * Get if model translatable
    * @return boolean
    */
    public static function isTranslatable(): bool{
        return false;
    }

    /**
     * Get Parent Class Name
     * @return string
     */
    public static function getDefiningClass()
    {
        return get_class();
    }

    /**
     *  Get Table Name
     * @return string
     */
    public static function getTableName()
    {
        return (new static)->getTable();
    }

    /**
     * Get Module Prefix
     * @return string
     */
    public static function getModule()
    {
        $moduleName = explode('\\', static::getClass())[1];
        return Config::get(Str::lower($moduleName) . '.modulePrefix');
    }

    /**
     * Get Current Class Name
     * @return string
     */
    public static function getClass()
    {
        return get_called_class();
    }

    /**
     * Get Plural Name Form
     * @return string
     */
    public static function getPlural()
    {
        return Str::plural(static::getSingular());
    }

    /**
     * Get Singular Name Form
     * @return string
     */
    public static function getSingular()
    {
        return Str::camel(class_basename(static::getClass()));
    }

    public static function doWithoutEvents(callable $callback)
    {
        return static::withoutEvents($callback);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (!in_array(Investigatable::class, class_uses($model->getClass())))
                return $model;

            /** @var User|null $user */
            $user = auth()->user();
            if ($user)
                $model[BaseFields::CREATED_BY] = $user[UserDefinition::ID];

            return $model;
        });

        static::updating(function ($model) {
            if (!in_array(Investigatable::class, class_uses($model->getClass())))
                return $model;

            /** @var User|null $user */
            $user = auth()->user();
            if ($user)
                $model[BaseFields::UPDATED_BY] = $user[UserDefinition::ID];

            return $model;
        });

//        static::deleting(function ($model) {
//            $classUsages = class_uses($model->getClass());
//            if (!in_array(Investigatable::class, $classUsages) || !in_array(SoftDeletes::class, $classUsages))
//                return $model;
//
//            /** @var User|null $user */
//            $user = auth()->user();
//            if ($user)
//                $model[BaseFields::DELETED_BY] = $user[UserDefinition::ID];
//
//            return $model;
//        });
    }
}
