<?php


namespace App\Services;

use Illuminate\Contracts\Container\BindingResolutionException;
use Modules\Arena\Services\ArenaService;
use Modules\Arena\Services\ArrangementService;
use Modules\Arena\Services\ParseTextFileService;
use Modules\Arena\Services\RowService;
use Modules\Arena\Services\SeatService;
use Modules\Arena\Services\SectionService;
use Modules\Arena\Services\VenueBoxService;
use Modules\Arena\Services\VenueDoorService;
use Modules\Arena\Services\VenueLevelService;
use Modules\Arena\Services\VenueSeatService;
use Modules\Arena\Services\VenueSeatViewService;
use Modules\Availability\Services\ReservationService;
use Modules\Availability\Services\SeatConfigurationService;
use Modules\Availability\Services\TicketCategoryAvailabilityService;
use Modules\Availability\Services\TicketTypeAvailabilityService;
use Modules\Availability\Services\TicketTypeSectionAvailabilityService;
use Modules\Cart\Services\CartService;
use Modules\Cart\Services\ConfigurationService;
use Modules\Cart\Services\ItemService;
use Modules\Common\Services\AttachmentService;
use Modules\Common\Services\CurrencyService;
use Modules\Common\Services\CustomFieldPossibleValueService;
use Modules\Common\Services\CustomFieldService;
use Modules\Common\Services\CustomImageService;
use Modules\Common\Services\LogService;
use Modules\Common\Services\NoteService;
use Modules\Common\Services\NotificationService;
use Modules\Common\Services\SitemapService;
use Modules\Configuration\Services\TaxConfigurationService;
use Modules\Configuration\Services\TaxService;
use Modules\Country\Services\CityService;
use Modules\Country\Services\CountryService;
use Modules\Customer\Services\CustomerService;
use Modules\Event\Services\AddOnService;
use Modules\Event\Services\LinkedEventService;
use Modules\Event\Services\EventService;
use Modules\Event\Services\MatchingService;
use Modules\Event\Services\SessionAttachmentService;
use Modules\Event\Services\SessionService;
use Modules\GiftVoucher\Services\GiftVoucherCodeService;
use Modules\Mailchimp\Services\EmailService;
use Modules\Mailchimp\Services\MessageService;
use Modules\NGenius\Services\NGeniusService;
use Modules\NGenius\Services\OutletService;
use Modules\Order\Services\AttendeeCheckInDetailService;
use Modules\Order\Services\AttendeeService;
use Modules\Order\Services\BoxOfficeService;
use Modules\Order\Services\CheckinLogService;
use Modules\Order\Services\FeeService;
use Modules\Order\Services\OrderService;
use Modules\Organizer\Services\OrganizerService;
use Modules\PromoCode\Services\PromoCodeService;
use Modules\PromoCode\Services\PromoCustomerPivotService;
use Modules\Review\Services\QuestionOptionService;
use Modules\Review\Services\SurveyAnswerService;
use Modules\Review\Services\SurveyService;
use Modules\Softix\Services\BasketsService;
use Modules\Softix\Services\CustomersService;
use Modules\Softix\Services\OrdersService;
use Modules\Softix\Services\PerformancesService;
use Modules\Softix\Services\SoftixService;
use Modules\Softix\Services\TokenLogService;
use Modules\Template\Services\TemplateService;
use Modules\Ticket\Services\CategoryService;
use Modules\Ticket\Services\CategoryGroupService;
use Modules\Ticket\Services\ExternalTicketsService;
use Modules\Ticket\Services\PriceService;
use Modules\Ticket\Services\TicketService;
use Modules\Ticket\Services\TypeSectionService;
use Modules\Ticket\Services\TypeService;
use Modules\Transaction\Services\CardInformationService;
use Modules\Transaction\Services\TransactionService;
use Modules\User\Services\CheckinUserService;
use Modules\User\Services\RoleSegmentService;
use Modules\User\Services\UserService;
use Modules\User\Services\UserVerificationService;
use Modules\Ticket\Services\TicketTypeStatusService;
use Modules\Transaction\Services\WalletService;

class ServiceHelper
{
    static function organizerService(): OrganizerService
    {
        return app()->make(OrganizerService::class);
    }

    static function notificationService(): NotificationService
    {
        return app()->make(NotificationService::class);
    }

    static function roleSegmentService(): RoleSegmentService
    {
        return app()->make(RoleSegmentService::class);
    }

    static function logService(): LogService
    {
        return app()->make(LogService::class);
    }

    static function attachmentService(): AttachmentService
    {
        return app()->make(AttachmentService::class);
    }

    static function noteService(): NoteService
    {
        return app()->make(NoteService::class);
    }

    static function currencyService(): CurrencyService
    {
        return app()->make(CurrencyService::class);
    }

    static function customFieldPossibleValueService(): CustomFieldPossibleValueService
    {
        return app()->make(CustomFieldPossibleValueService::class);
    }

    static function customFieldService(): CustomFieldService
    {
        return app()->make(CustomFieldService::class);
    }

    static function countryService(): CountryService
    {
        return app()->make(CountryService::class);
    }

    static function cityService(): CityService
    {
        return app()->make(CityService::class);
    }

    static function eventService(): EventService
    {
        return app()->make(EventService::class);
    }

    static function seatService(): SeatService
    {
        return app()->make(SeatService::class);
    }

    static function sessionService(): SessionService
    {
        return app()->make(SessionService::class);
    }

    static function sessionAttachmentService(): SessionAttachmentService
    {
        return app()->make(SessionAttachmentService::class);
    }

    static function sectionService(): SectionService
    {
        return app()->make(SectionService::class);
    }

    static function ticketTypeService(): TypeService
    {
        return app()->make(TypeService::class);
    }

    static function ticketTypeSectionService(): TypeSectionService
    {
        return app()->make(TypeSectionService::class);
    }

    static function ticketCategoryService(): CategoryService
    {
        return app()->make(CategoryService::class);
    }

    static function ticketPriceService(): PriceService
    {
        return app()->make(PriceService::class);
    }

    static function arenaService(): ArenaService
    {
        return app()->make(ArenaService::class);
    }

    static function arrangementService(): ArrangementService
    {
        return app()->make(ArrangementService::class);
    }

    static function userService(): UserService
    {
        return app()->make(UserService::class);
    }

    static function customerService(): CustomerService
    {
        return app()->make(CustomerService::class);
    }

    static function orderService(): OrderService
    {
        return app()->make(OrderService::class);
    }

    static function attendeeService(): AttendeeService
    {
        return app()->make(AttendeeService::class);
    }

    static function ticketService(): TicketService
    {
        return app()->make(TicketService::class);
    }

    static function externalTicketService(): ExternalTicketsService
    {
        return app()->make(ExternalTicketsService::class);
    }

    static function cartService(): CartService
    {
        return app()->make(CartService::class);
    }

    static function checkinLogService(): CheckinLogService
    {
        return app()->make(CheckinLogService::class);
    }

    static function cartItemService(): ItemService
    {
        return app()->make(ItemService::class);
    }

    static function cartConfigurationService(): ConfigurationService
    {
        return app()->make(ConfigurationService::class);
    }

    static function rowService(): RowService
    {
        return app()->make(RowService::class);
    }

    static function categoryAvailabilityService(): TicketCategoryAvailabilityService
    {
        return app()->make(TicketCategoryAvailabilityService::class);
    }

    static function typeAvailabilityService(): TicketTypeAvailabilityService
    {
        return app()->make(TicketTypeAvailabilityService::class);
    }

    static function typeSectionAvailabilityService(): TicketTypeSectionAvailabilityService
    {
        return app()->make(TicketTypeSectionAvailabilityService::class);
    }

    static function reservationService(): ReservationService
    {
        return app()->make(ReservationService::class);
    }

    static function taxService(): TaxService
    {
        return app()->make(TaxService::class);
    }

    static function taxConfigurationService(): TaxConfigurationService
    {
        return app()->make(TaxConfigurationService::class);
    }

    static function orderItemService(): \Modules\Order\Services\ItemService
    {
        return app()->make(\Modules\Order\Services\ItemService::class);
    }

    static function orderFeeService(): FeeService
    {
        return app()->make(FeeService::class);
    }

    static function outletService(): OutletService
    {
        return app()->make(OutletService::class);
    }

    static function paymentGatewayService(): NGeniusService
    {
        return app()->make(NGeniusService::class);
    }

    static function transactionService(): TransactionService
    {
        return app()->make(TransactionService::class);
    }

    static function transactionLogService(): \Modules\Transaction\Services\LogService
    {
        return app()->make(\Modules\Transaction\Services\LogService::class);
    }

    static function userVerificationService(): UserVerificationService
    {
        return app()->make(UserVerificationService::class);
    }

    static function cardInformationService(): CardInformationService
    {
        return app()->make(CardInformationService::class);
    }

    static function softixCustomerService(): CustomersService
    {
        return app()->make(CustomersService::class);
    }

    static function softixBasketService(): BasketsService
    {
        return app()->make(BasketsService::class);
    }

    static function seatConfigurationService(): SeatConfigurationService
    {
        return app()->make(SeatConfigurationService::class);
    }

    static function softixEventService(): PerformancesService
    {
        return app()->make(PerformancesService::class);
    }

    static function softixOrderService(): OrdersService
    {
        return app()->make(OrdersService::class);
    }

    static function attendeeCheckInDetailService(): AttendeeCheckInDetailService
    {
        return app()->make(AttendeeCheckInDetailService::class);
    }

    static function surveyService(): SurveyService
    {
        return app()->make(SurveyService::class);
    }

    static function questionOptionService(): QuestionOptionService
    {
        return app()->make(QuestionOptionService::class);
    }

    static function parseTextFileService(): ParseTextFileService
    {
        return app()->make(ParseTextFileService::class);
    }

    static function checkinUserService(): CheckinUserService
    {
        return app()->make(CheckinUserService::class);
    }

    static function ticketTypeStatusService(): TicketTypeStatusService
    {
        return app()->make(TicketTypeStatusService::class);
    }

    static function ticketCategoryGroupService(): CategoryGroupService
    {
        return app()->make(CategoryGroupService::class);
    }

    static function eventCategoriesService(): \Modules\Listing\Services\CategoryService
    {
        return app()->make(\Modules\Listing\Services\CategoryService::class);
    }

    static function promoCodeService(): PromoCodeService
    {
        return app()->make(PromoCodeService::class);
    }

    static function WalletService(): WalletService
    {
        return app()->make(WalletService::class);
    }

    static function addOnService(): AddOnService
    {
        return app()->make(AddOnService::class);
    }

    static function promoCustomerPivotService(): PromoCustomerPivotService
    {
        return app()->make(PromoCustomerPivotService::class);
    }

    static function venueSeatService(): VenueSeatService
    {
        return app()->make(VenueSeatService::class);
    }

    static function venueDoorService(): VenueDoorService
    {
        return app()->make(VenueDoorService::class);
    }

    static function venueLevelService(): VenueLevelService
    {
        return app()->make(VenueLevelService::class);
    }

    static function GiftVoucherCodeService(): GiftVoucherCodeService
    {
        return app()->make(GiftVoucherCodeService::class);
    }

    static function templateService(): TemplateService
    {
        return app()->make(TemplateService::class);
    }

    static function surveyAnswerService(): SurveyAnswerService
    {
        return app()->make(SurveyAnswerService::class);
    }

    static function tokenLogService(): TokenLogService
    {
        return app()->make(TokenLogService::class);
    }

    static function BasketsSoftixService(): BasketsService
    {
        return app()->make(BasketsService::class);
    }

    static function MatchingService() : MatchingService {
        return app()->make(MatchingService::class);
    }

    static function boxOfficeService(): BoxOfficeService {
        return app()->make(BoxOfficeService::class);
    }

    static function venueSeatViewService(): VenueSeatViewService {
        return app()->make(VenueSeatViewService::class);
    }

    static function venueBoxService(): VenueBoxService {
        return app()->make(VenueBoxService::class);
    }

    /**
     * @throws BindingResolutionException
     */
    public static function customImageService(): CustomImageService {
        return app()->make(CustomImageService::class);
    }

    public static function messageService() : MessageService {
        return app()->make(MessageService::class);
    }

    public static function emailService() : EmailService {
        return app()->make(EmailService::class);
    }

    public static function linkedEventService() : LinkedEventService {
        return app()->make(LinkedEventService::class);
    }

    public static function sitemapService() : SitemapService {
        return app()->make(SitemapService::class);
    }
}
