<?php


namespace App\Services;


use App\Repositories\Contracts\RepositoryInterface;
use App\Services\Contracts\ServiceInterface;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseService implements ServiceInterface
{
    protected $primaryRepository;

    /**
     * BaseService constructor.
     * @param RepositoryInterface $primaryRepository Model's Primary Repository
     * @throws Exception
     */
    public function __construct(RepositoryInterface $primaryRepository)
    {
        if ($primaryRepository == null)
            throw new Exception('Fatal Error, Primary Repository Is Required');

        $this->primaryRepository = $primaryRepository;
    }

    /**
     * @param array $order
     * @param array $columns
     * @param bool $includeDefused
     * @return mixed|null
     */
    public function all($order = array(), $columns = array('*'), bool $includeDefused = false)
    {
        return $this->primaryRepository->all($order, $columns, $includeDefused);
    }

    /**
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $includeTrashed
     * @param bool $includeDefused
     * @param array $columns
     * @return mixed|null
     */
    public function paginate($perPage = 15, $order = array(), $conditions = array(), $includeTrashed = false, $includeDefused = false, $columns = array('*'))
    {
        return $this->primaryRepository->paginate($perPage, $order, $conditions, $includeTrashed, $includeDefused, $columns);
    }

    /**
     * @param array $tables
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $includeTrashed
     * @param bool $includeDefused
     * @param array $columns
     * @return mixed|null
     */
    public function paginateWith($tables = array(), $perPage = 15, $order = array(), $conditions = array(), $includeTrashed = false, $includeDefused = false, $columns = array('*'))
    {
        return $this->primaryRepository->paginateWith($tables, $perPage, $order, $conditions, $includeTrashed, $includeDefused, $columns);
    }

    /**
     * @param array $conditions
     * @param array $order
     * @param array $columns
     * @return mixed
     */
    public function where($conditions = array(), $order = array(), $columns = array('*'))
    {
        return $this->primaryRepository->where($conditions, $order, $columns);
    }

    /**
     * @param string $field
     * @param array $items
     * @param array $order
     * @param array $columns
     * @return mixed
     * @throws Exception
     */
    public function whereIn(string $field, array $items = array(), $order = array(), $columns = array('*')): Collection
    {
        return $this->primaryRepository->whereIn($field, $items, $order, $columns);
    }

    /**
     * @param array $tables
     * @param array $conditions
     * @param array $order
     * @param array $columns
     * @param bool $includeDefused
     * @return mixed|null
     */
    public function with($tables = array(), $conditions = array(), $order = array(), $columns = array('*'), bool $includeDefused = false)
    {
        return $this->primaryRepository->with($tables, $conditions, $order, $columns, $includeDefused);
    }

    /**
     * @param array $order
     * @param array $conditions
     * @param array $columns
     * @return mixed
     */
    public function getTrashed($order = array(), $conditions = array(), $columns = array('*'))
    {
        return $this->primaryRepository->getTrashed($order, $conditions, $columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed|null
     */
    public function find($id, $columns = array('*'))
    {
        return $this->primaryRepository->find($id, $columns);
    }

    /**
     * @param $field
     * @param $value
     * @param array $order
     * @param array $columns
     * @param bool $disableCache
     * @return mixed|null
     */
    public function findByProperty($field, $value, $order = array(), $columns = array('*'), bool $disableCache = false)
    {
        return $this->primaryRepository->findByProperty($field, $value, $order, $columns, $disableCache);
    }

    /**
     * @param $conditions
     * @param array $columns
     * @return mixed|null
     */
    public function first($conditions, $columns = array('*'))
    {
        return $this->primaryRepository->first($conditions, $columns);
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): ?Model
    {
        return $this->primaryRepository->create($data);
    }

    /**
     * @param Model $model
     * @return bool|mixed
     */
    public function save(Model $model)
    {
        return $this->primaryRepository->save($model);
    }

    /**
     * @param array $data
     * @param $attributeValues
     * @param string $attribute
     * @return bool|mixed
     */
    public function update(array $data, $attributeValues, $attribute = 'id')
    {
        return $this->primaryRepository->update($data, $attributeValues, $attribute);
    }

    /**
     * @param $conditions
     * @return bool
     */
    public function softDeleteWhere($conditions): bool
    {
        return $this->primaryRepository->softDeleteWhere($conditions);
    }

    /**
     * @param array|int $ids
     * @return bool
     */
    public function softDelete($ids): bool
    {
        return $this->primaryRepository->softDelete($ids);
    }

    /**
     * @param $object
     * @return bool
     * @throws Exception
     */
    public function softDeleteObject($object): bool
    {
        try {
            return $this->primaryRepository->softDeleteObject($object);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $object
     * @return Model
     * @throws Exception
     */
    public function restoreObject(&$object): Model
    {
        return $this->primaryRepository->restoreObject($object);
    }

    /**
     * @param $ids
     * @return bool
     */
    public function forceDelete($ids): bool
    {
        return $this->primaryRepository->forceDelete($ids);
    }

    /**
     * @param $conditions
     * @return bool
     */
    public function forceDeleteWhere($conditions): bool
    {
        return $this->primaryRepository->forceDeleteWhere($conditions);
    }

    /**
     * @param $object
     * @return bool
     * @throws Exception
     */
    public function forceDeleteObject($object): bool
    {
        try {
            return $this->primaryRepository->forceDeleteObject($object);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $ids
     * @return bool|mixed
     */
    public function restore($ids)
    {
        return $this->primaryRepository->restore($ids);
    }

    /**
     * @param $field
     * @param $value
     * @return bool
     */
    public function has($field, $value)
    {
        return $this->primaryRepository->has($field, $value);
    }

    /**
     * Deletes all data in a table
     * @return mixed
     */
    public function truncate()
    {
        return $this->primaryRepository->truncate();
    }

    /**
     * @param $relation
     * @param $model
     * @param array $related
     * @throws Exception
     */
    public function syncRelation($relation, $model, array $related)
    {
        $this->primaryRepository->syncRelation($relation, $model, $related);
    }

    /**
     * @param Model $object
     * @return bool
     * @throws Exception
     */
    public function defuseObject(Model $object): bool
    {
        return $this->primaryRepository->defuseObject($object);
    }

    /**
     * @param Model $object
     * @return bool
     * @throws Exception
     */
    public function activateObject(Model $object): bool
    {
        return $this->primaryRepository->activateObject($object);
    }
}
