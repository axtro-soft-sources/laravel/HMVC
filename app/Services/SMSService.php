<?php


namespace App\Services;


use Twilio\Rest\Client;

class SMSService
{
    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param String $recipients string or array of phone number of recepient
     * @return void
     */
    public static function sendMessage($message, $recipients)
    {
        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients,
                ['from' => $twilio_number, 'body' => $message]);
        } catch (\Exception $exception) {
            error_log($exception);
        }
    }
}
