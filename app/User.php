<?php

namespace App;

use App\Entities\BaseFields;
use App\Entities\Traits\Defusable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Common\Entities\Traits\Attachable;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryDefinition;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Entities\CustomerDefinition;
use Modules\Event\Entities\Event;
use Modules\Event\Entities\ModelHasEventPermissionDefinition;
use Modules\Order\Entities\CheckInLog;
use Modules\Organizer\Entities\Organizer;
use Modules\User\Entities\EmailGroup;
use Modules\User\Entities\EmailGroupUserDefinition;
use Modules\User\Entities\UserDefinition;
use Modules\User\Entities\UserVerification;
use Modules\User\Entities\UserVerificationDefinition;
use Propaganistas\LaravelPhone\PhoneNumber;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package App
 * @property Customer customer
 */
class User extends Authenticatable implements JWTSubject, HasMedia
{
    use HasFactory, Notifiable, HasRoles, SoftDeletes, Defusable, Attachable;

    //region ORM members

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = UserDefinition::FILLABLES;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        UserDefinition::PASSWORD,
        'remember_token',
        BaseFields::CREATED_AT,
        BaseFields::UPDATED_AT,
        BaseFields::DELETED_AT,
        UserDefinition::SEND_MAILS_UNDER_SYSTEM_ACCOUNT
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //endregion

    //region AuthSubject members

    public static function getClass()
    {
        return User::class;
    }

    /**
     * Get Plural Name Form
     * @return string
     */
    public static function getPlural()
    {
        return 'users';
    }
    //endregion

    //region Properties

    /**
     * Get Module Prefix
     * @return string
     */
    public static function getModule()
    {
        return 'user';
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->hasRole(['Administrator', 'Super Admin']);
    }

    /**
     * @return bool
     * @author Waseem Alkhen
     */
    public function isDistributor(): bool
    {
        return $this->hasRole(Roles::DISTRIBUTOR);
    }

    public function isVerified()
    {
        return $this->isVerifiedEmailAddress() || $this->isVerifiedMobileNumber();
    }

    public function isVerifiedEmailAddress()
    {
        return $this[UserDefinition::EMAIL_VERIFIED_AT];
    }

    public function isVerifiedMobileNumber()
    {
        return $this[UserDefinition::PHONE_NUMBER_VERIFIED_AT] != null;
    }

    //endregion

    //region Mutators

    public function isStaff()
    {
        return !$this->isCustomer();
    }

    public function isCustomer()
    {
        return $this->hasRole(Roles::CUSTOMER);
    }

    /**
     * @return bool
     * @author Waseem Alkhen
     */
    public function isBoxOfficer(): bool
    {
        return $this->hasRole(Roles::BOX_OFFICE_EMPLOYEE);
    }


    //endregion

    //region Relations

    public function isSociallyAuthenticated()
    {
        return $this[UserDefinition::FACEBOOK_ACCOUNT_TOKEN] || $this[UserDefinition::GOOGLE_ACCOUNT_TOKEN];
    }

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        if (!empty($password)) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function setPhoneNumberAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes[UserDefinition::PHONE_NUMBER] = PhoneNumber::make($value,
                $this->phoneNumberCountry[CountryDefinition::ISO_ALPHA2_CODE])->formatE164();
        }
    }

    public function getPhoneNumberAttribute()
    {
        $value = $this->attributes[UserDefinition::PHONE_NUMBER];
        if (!empty($value)) {
            return PhoneNumber::make($value, $this->phoneNumberCountry[CountryDefinition::ISO_ALPHA2_CODE])->formatNational();
        } else {
            return $value;
        }
    }

    public function phoneNumberCountry()
    {
        return $this->belongsTo(Country::class, UserDefinition::PHONE_NUMBER_COUNTRY_ID);
    }

    public function activeVerification(string $type)
    {
        return $this->verifications()->where(UserVerificationDefinition::TYPE, $type)
            ->where(UserVerificationDefinition::VALID_TILL, '>=', now())
            ->first();
    }

    //endregion

    //region Public members

    public function verifications()
    {
        return $this->hasMany(UserVerification::class);
    }

    /**
     * @return BelongsToMany
     */
    public function emailGroups(): BelongsToMany
    {
        return $this->belongsToMany(EmailGroup::class, EmailGroupUserDefinition::TABLE_NAME);
    }

    public function customer(): HasOne
    {
        return $this->hasOne(Customer::class, CustomerDefinition::USER_ID);
    }


    public function checkInLogs()
    {
        return $this->morphMany(CheckInLog::class, 'checkable');
    }

    /**
     * Get all the events that are assigned this event.
     * @return BelongsToMany
     */
    public function events(): BelongsToMany
    {
        return $this->morphToMany(Event::class, 'model', ModelHasEventPermissionDefinition::TABLE_NAME);
    }

    //endregion

    public function organizers(): BelongsToMany
    {
        return $this->belongsToMany(Organizer::class);
    }
}
