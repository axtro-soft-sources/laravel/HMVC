<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * @var array order for queries
     */
    protected $defaultOrder;
    /*
     * @var BaseModel $persistentClass
     */
    protected $persistentClass;

    /**
     * Repository constructor.
     * @param $persistentClass
     * @param $defaultOrder
     */
    protected function __construct($persistentClass, $defaultOrder = ['id' => 'desc'])
    {
        $this->persistentClass = $persistentClass;
        $this->defaultOrder = $defaultOrder;
    }

    /**
     * @param array $order
     * @param array $columns
     * @param $
     * @param bool $includeDefused
     * @return mixed|null
     * @throws Exception
     */
    public function all($order = array(), $columns = array('*'), bool $includeDefused = false)
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            //TODO: Implement order and with
            if (!$includeDefused)
                $data = $this->persistentClass::all();
            else
                $data = $this->persistentClass::withDefused()->orderBy($order)->select($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $includeTrashed
     * @param bool $includeDefused
     * @param array $columns
     * @return mixed|null
     * @throws Exception
     */
    public function paginate($perPage = 15, $order = array(), $conditions = array(), $includeTrashed = false, $includeDefused = false, $columns = array('*'))
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            $querySet = $this->persistentClass::where($conditions);
            if ($includeTrashed)
                $querySet->withTrashed();
            if ($includeDefused)
                $querySet->withDefused();

            $querySet = $this->buildNestedOrder($querySet, $order);
            $data = $querySet->paginate($perPage, $columns);
        } catch (Exception $e) {
            error_log($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param Builder $querySet
     * @param array $order
     * @return Builder
     */
    protected function buildNestedOrder(Builder &$querySet, $order = array()): Builder
    {
        if (count($order) <= 0)
            return $querySet;

        foreach ($order as $orderBy => $orderDir) {
            if (strpos(strval($orderBy), '.') !== false) {
                $orderBySegments = explode('.', $orderBy);
                $targetField = array_pop($orderBySegments);
                $targetRelation = implode('.', $orderBySegments);

                $querySet->with([$targetRelation => function ($query) use ($targetField, $orderDir) {
                    $query->orderBy($targetField, $orderDir);
                }]);
            } else {
                $querySet->orderBy($orderBy, $orderDir);
            }
        }

        return $querySet;
    }

    /**
     * @param array $tables
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $includeTrashed
     * @param bool $includeDefused
     * @param array $columns
     * @return mixed|null
     * @throws Exception
     */
    public function paginateWith($tables = array(), $perPage = 15, $order = array(), $conditions = array(), $includeTrashed = false, $includeDefused = false, $columns = array('*'))
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        try {
            $query = $this->persistentClass::where($conditions);

            if ($includeTrashed)
                $query->withTrashed();

            if ($includeDefused)
                $query->withDefused();

            $this->buildNestedOrder($query, $order);

            foreach ($tables as $tableName => $tableConditions) {
                $query->with([$tableName => function ($query) use (&$tableConditions) {
                    $query->where($tableConditions);
                }]);
            }

            $data = $query->paginate($perPage, $columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param array $conditions
     * @param array $order
     * @param array $columns
     * @return mixed
     * @throws Exception
     */
    public function where($conditions = array(), $order = array(), $columns = array('*'))
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            $querySet = $this->persistentClass::where($conditions);

            $this->buildNestedOrder($querySet, $order);

            $data = $querySet->get($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param string $field
     * @param array $items
     * @param array $order
     * @param array $columns
     * @return mixed
     * @throws Exception
     */
    public function whereIn(string $field, array $items = array(), $order = array(), $columns = array('*'))
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            $querySet = $this->persistentClass::whereIn($field, $items);

            $this->buildNestedOrder($querySet, $order);

            $data = $querySet->get($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param array $tables
     * @param array $conditions
     * @param array $order
     * @param array $columns
     * @param bool $includeDefused
     * @return mixed|null
     * @throws Exception
     */
    public function with($tables = array(), $conditions = array(), $order = array(), $columns = array('*'), bool $includeDefused = false)
    {
        $data = null;
        $queryOrder = empty($order) ? $this->defaultOrder : $order;
        try {
            $query = $this->persistentClass::where($conditions);
            if ($includeDefused)
                $query->withDefused();

            foreach ($tables as $tableName => $tableConditions) {
                $query->with([$tableName => function ($query) use (&$tableConditions) {
                    $query->where($tableConditions);
                }]);

            }
            $this->buildNestedOrder($query, $queryOrder);
            $data = $query->get();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        //return (count($data) == 1) ? $data[0] : $data;
        //Will always return a collection
        return $data;
    }

    /**
     * @param array $order
     * @param array $conditions
     * @param array $columns
     * @return mixed
     * @throws Exception
     */
    public function getTrashed($order = array(), $conditions = array(), $columns = array('*'))
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            $querySet = $this->persistentClass::onlyTrashed()->where($conditions);

            $this->buildNestedOrder($querySet, $order);

            $data = $querySet->get($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed|null
     * @throws Exception
     */
    public function find($id, $columns = array('*'))
    {
        $data = null;
        try {
            $data = $this->persistentClass::find($id, $columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param $conditions
     * @param array $columns
     * @return mixed|null
     * @throws Exception
     */
    public function first($conditions, $columns = array('*'))
    {
        $data = null;
        try {
            $data = $this->persistentClass::where($conditions)->first($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * @param array $data
     * @return Model
     * @throws Exception
     */
    public function create(array $data): ?Model
    {
        try {
            return $this->persistentClass::create($data);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param Model $model
     * @return bool|mixed
     * @throws Exception
     */
    public function save(Model $model)
    {
        try {
            return $model->save();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param $attributeValues
     * @param string $attribute
     * @return bool|mixed
     * @throws Exception
     */
    public function update(array $data, $attributeValues, $attribute = 'id')
    {
        try {
            return $this->persistentClass::whereIn($attribute, $attributeValues)->update($data);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param array|int $ids
     * @return bool
     * @throws Exception
     */
    public function softDelete($ids): bool
    {
        try {
            $this->persistentClass::whereIn('id', $ids)->delete();
            return true;
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $conditions
     * @return bool
     * @throws Exception
     */
    public function softDeleteWhere($conditions): bool
    {
        try {
            $this->persistentClass::where($conditions)->delete();
            return true;
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $object
     * @return Model
     * @throws Exception
     */
    public function restoreObject(&$object): Model
    {
        $object->restore();
        return $object;
    }

    /**
     * @param $object
     * @return bool
     * @throws Exception
     */
    public function softDeleteObject($object): bool
    {
        try {
            return $object->delete();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param array|int $ids
     * @return bool
     * @throws Exception
     */
    public function forceDelete($ids): bool
    {
        try {
            return $this->persistentClass::destroy($ids);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $conditions
     * @return bool
     * @throws Exception
     */
    public function forceDeleteWhere($conditions): bool
    {
        try {
            $this->persistentClass::where($conditions)->forceDelete();
            return true;
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $object
     * @return bool
     * @throws Exception
     */
    public function forceDeleteObject($object): bool
    {
        try {
            return $object->forceDelete();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $ids
     * @return bool|mixed
     * @throws Exception
     */
    public function restore($ids)
    {
        try {
            $this->persistentClass::withTrashed()->whereIn('id', $ids)->restore();
            return true;
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param $field
     * @param $value
     * @return bool
     * @throws Exception
     */
    public function has($field, $value)
    {
        return (count($this->findByProperty($field, $value)) > 0);
    }

    /**
     * @param $field
     * @param $value
     * @param array $order
     * @param array $columns
     * @param bool $disableCache
     * @return mixed|null
     * @throws Exception
     */
    public function findByProperty($field, $value, $order = array(), $columns = array('*'), bool $disableCache = false)
    {
        $order = empty($order) ? $this->defaultOrder : $order;
        $data = null;
        try {
            $querySet = $this->persistentClass::where($field, $value);
            if ($disableCache)
                $querySet = $querySet->disableCache();

            $querySet = $querySet->orderBy(key($order), $order[key($order)]);
            $data = $querySet->get($columns);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
        return $data;
    }

    /**
     * Deletes all data in a table
     * @return mixed
     * @throws Exception
     */
    public function truncate()
    {
        try {
            $this->persistentClass::truncate();
            return true;
        } catch (Exception $e) {
            report($e);
            throw $e;
        }

    }

    /**
     * @param $relation
     * @param $model
     * @param array $related
     * @throws Exception
     */
    public function syncRelation($relation, $model, array $related)
    {
        try {
            $model->$relation()->sync($related);
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param Model $object
     * @return bool
     * @throws Exception
     */
    public function defuseObject(Model $object): bool
    {
        try {
            return $object->defuse();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * @param Model $object
     * @return bool
     * @throws Exception
     */
    public function activateObject(Model $object): bool
    {
        try {
            return $object->activate();
        } catch (Exception $e) {
            report($e);
            throw $e;
        }
    }

    public function decrement(Model $object, string $attribute, int $value = 1){
        $object->decrement($attribute, $value);
        $object->save();
    }
}
