<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    // If you want to add an array type for this variable you MUST change the type of this property in all classes that are extended from this class
    protected $routeParametersToValidate = [];
    protected $queryParametersToValidate = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    public function all($keys = null)
    {
        $data = parent::all();

        foreach ($this->routeParametersToValidate as $routeParameter) {
            $val = $this->route($routeParameter);
            if ($val)
                $data[$routeParameter] = $val;
        }

        foreach ($this->queryParametersToValidate as $queryParameter) {
            $val = $this->query($queryParameter);
            if ($val)
                $data[$queryParameter] = $val;
        }

        return $data;
    }
}
