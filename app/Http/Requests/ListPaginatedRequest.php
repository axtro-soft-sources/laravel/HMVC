<?php


namespace App\Http\Requests;


use Illuminate\Validation\Rule;

abstract class ListPaginatedRequest extends BaseFormRequest
{
    protected $queryParametersToValidate = [
        'query',
        'page',
        'page_size',
        'order_by',
        'order_dir',
        'include_defused'
    ];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [
            'query' => 'nullable|string',
            'page' => 'nullable|integer|min:1',
            'page_size' => 'nullable|integer|min:1'
        ];

        if ($this->isDefusable())
            $rules['include_defused'] = 'nullable|boolean';

        if ($this->isSortable()) {
            $rules['order_by'] = [
                'nullable',
                Rule::in($this->getSortables())
            ];

            $rules['order_dir'] = [
                'nullable',
                Rule::in(['asc', 'desc'])
            ];
        }

        return $rules;
    }

    public function isDefusable()
    {
        return true;
    }

    public function isSortable()
    {
        return false;
    }

    public function getSortables()
    {
        return [];
    }
}
