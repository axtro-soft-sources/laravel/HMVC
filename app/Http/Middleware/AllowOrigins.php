<?php

namespace App\Http\Middleware;

use App\Services\ServiceHelper;
use Closure;
use Modules\Common\Entities\Currency;

class AllowOrigins
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


         header('Access-Control-Allow-Origin: *');
         header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
         header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Authorization, Origin, X-Socket-ID, X-Display-Currency, Locale');


        $targetCurrency = request()->header('X-Display-Currency', Currency::DEFAULT_CURRENCY);
        $isValidCurrency = ServiceHelper::outletService()->isSupportedCurrency($targetCurrency);
        $displayCurrency = $isValidCurrency ? $targetCurrency : Currency::DEFAULT_CURRENCY;

        $response = $next($request);


        header("X-Display-Currency: $displayCurrency");


        return $response;
    }
}
