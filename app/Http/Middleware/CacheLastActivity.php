<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Cache;

class CacheLastActivity
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($id = auth()->id()) {
            $expiresAt = Carbon::now()->addMinutes(5);
            Cache::put('user-' . $id . '-is-online', true, $expiresAt);
        }
        return $next($request);
    }
}
