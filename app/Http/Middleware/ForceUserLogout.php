<?php

namespace App\Http\Middleware;

use App\Entities\Messaging\Response;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ForceUserLogout
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($id = auth()->id()) {
            $forceLogoutKey = 'force-user-' . $id . '-logout';
            if (Cache::has($forceLogoutKey)) {
                Cache::forget($forceLogoutKey);
                auth()->invalidate();
                // Terminate continuing request
                return response()->json(new Response(null, 'Session Expired'), 401);
            }
        }

        return $next($request);
    }
}
