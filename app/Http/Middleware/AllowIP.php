<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class AllowIP
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws AuthorizationException
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() && auth()->user()->allowed_ips && !in_array($request->ip(), explode(';', auth()->user()->allowed_ips)))
            throw new AuthorizationException('Blocked IP.', 433);
        return $next($request);
    }
}
