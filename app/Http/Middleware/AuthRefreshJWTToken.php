<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthRefreshJWTToken
{
    protected $except_urls = [
        'auth/login',
    ];

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* $refreshed = null;
         if ($request->hasHeader('Authorization') && Str::start($request->path(), '/api') && !Str::contains($request->path(), 'auth/login')) {
             try {
                 $token = JWTAuth::parseToken();
                 $token->authenticate();
             } catch (TokenExpiredException $e) {
                 try {
                     $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                     JWTAuth::setToken($refreshed);

                     $request->user = JWTAuth::authenticate($refreshed);
                     $request->headers->set('Authorization', 'Bearer ' . $refreshed);
                 } catch (JWTException $e) {
                     //PASS
                 }
             } catch (JWTException $e) {
                 //PASS
             }
         }*/

        /* @var Response $response */
        $response = $next($request);
        /*if ($refreshed) {
            $response->headers->set('Authorization', 'Bearer ' . $refreshed);
        }*/
        return $response;
    }

}
