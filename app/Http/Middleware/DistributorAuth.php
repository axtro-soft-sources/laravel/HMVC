<?php

namespace App\Http\Middleware;

use App\Roles;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class DistributorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @author Waseem Alkhen, Waleed Alrashed
     */
    public function handle(Request $request, Closure $next): mixed
    {

        $user = auth()->user();

        if (!isset($user)) {
            return $next($request);
        }

        $userRoles = $user->roles()->get()->pluck('name')->toArray();

        $apiUrl = Str::title(explode('/', $request->getPathInfo())[2]);

        if ($apiUrl !== Roles::DISTRIBUTOR && in_array(Roles::DISTRIBUTOR, $userRoles, true)) {
            return response()->json([
                'message' => 'Not Allowed'
            ]);
        }

        return $next($request);

    }
}
