<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\User\Entities\PermissionDefinition;
use Modules\User\Entities\RoleDefinition;
use Modules\User\Entities\UserDefinition;
use Sentry\State\Scope;
use function Sentry\configureScope;

class SentryContext
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && app()->bound('sentry')) {
            configureScope(static function (Scope $scope): void {
                $authUser = auth()?->user();
                $scope->setUser([
                    'id' => auth()->user()[UserDefinition::ID],
                    'email' => auth()->user()[UserDefinition::EMAIL],
                    'name' => auth()->user()[UserDefinition::NAME],
                    'roles' => $authUser?->roles()->pluck(RoleDefinition::NAME),
                    'all_permissions' => $authUser?->getAllPermissions()->pluck(PermissionDefinition::NAME)
                ]);
            });
        }

        return $next($request);
    }
}
