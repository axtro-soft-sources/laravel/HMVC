<?php

namespace App\Http\Controllers;

use App\Entities\Messaging\Response;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserPasswordResetRequest;
use App\Services\ServiceHelper;
use App\Utils\DateUtil;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Modules\User\Entities\CheckinUser;
use Modules\User\Entities\UserDefinition;
use Modules\User\Http\Requests\RegisterUserRequest;
use Modules\User\Services\UserService;
use Modules\User\Transformers\UserIdentityResource;
use Modules\User\Transformers\UserResource;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends BaseController
{
    /** @var UserService */
    private UserService $service;

    /**
     * Create a new AuthController instance.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->middleware('auth:api', ['except' => [
            'login', 'facebookLogin', 'googleLogin',
            'requestNewPassword', 'register', 'resetPassword'
        ]]);
        $this->service = $service;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(new Response(null, 'Wrong credentials.'), 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @param bool $isMobile
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->ok([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL()
        ]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws GuzzleException
     */
    public function facebookLogin(Request $request)
    {
        $request->validate([
            'access_token' => 'required|string'
        ]);

        $accessToken = $request->get('access_token');
        try {
            $graphResponse = $this->service->verifyFacebookAccessToken($accessToken);

            $accountId = $graphResponse['id'];
            $emailAddress = $graphResponse['email'];

            $user = $this->service->findUserViaFacebookAccount($accountId);

            if (!$user) {
                return $this->notFound("You don't seem to have an account on TicketFolks, please register first");
            } else if (strtolower($user[UserDefinition::EMAIL]) != strtolower($emailAddress)) {
                $user[UserDefinition::EMAIL] = $emailAddress;
                $user[UserDefinition::EMAIL_VERIFIED_AT] = now();

                $this->service->save($user);
            }

            $token = JWTAuth::fromUser($user);

            return $this->respondWithToken($token);
        } catch (Exception $exception) {
            return $this->badRequest('Invalid token');
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws GuzzleException
     */
    public function googleLogin(Request $request)
    {
        $request->validate([
            'access_token' => 'required|string'
        ]);

        $accessToken = $request->get('access_token');
        try {
            $response = $this->service->verifyGoogleAccessToken($accessToken);
            $accountId = $response['sub'];
            $emailAddress = $response['email'];

            $user = $this->service->findUserViaGoogleAccount($accountId);

            if (!$user) {
                return $this->notFound("You don't seem to have an account on TicketFolks, please register first");
            } else if (strtolower($user[UserDefinition::EMAIL]) != strtolower($emailAddress)) {
                $user[UserDefinition::EMAIL] = $emailAddress;
                $user[UserDefinition::EMAIL_VERIFIED_AT] = now();

                $this->service->save($user);
            }

            $token = JWTAuth::fromUser($user);

            return $this->respondWithToken($token);
        } catch (Exception $exception) {
            return $this->badRequest('Invalid Token');
        }
    }

    /**
     * @param RegisterUserRequest $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws GuzzleException
     */
    public function register(RegisterUserRequest $request)
    {
        $emailAddress = $request->get('email');
        $userFirstName = $request->get('first_name');
        $userLastName = $request->get('last_name');
        $password = $request->get('password');
        $phoneNumber = $request->get('phone_number');
        $dateOfBirth = Carbon::createFromFormat(DateUtil::DATE_FORMAT_IN, $request->get('date_of_birth'));
        $phoneNumberCountry = ServiceHelper::countryService()->getCountryByIso2Code($request->get('phone_number_country'));
        $nationality = ServiceHelper::countryService()->find($request->get('nationality_id'));
        $facebookAccessToken = $request->get('facebook_access_token');
        $googleAccessToken = $request->get('google_access_token');

        $persistedUser = $this->service->addClientUser(
            $emailAddress,
            $userFirstName,
            $userLastName,
            $password,
            $phoneNumber,
            $phoneNumberCountry,
            $nationality,
            $dateOfBirth,
            $facebookAccessToken,
            $googleAccessToken
        );

        return $this->ok(new UserResource($persistedUser));
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        $user = auth()->user();

        $resource = new UserIdentityResource($user);
        return $this->ok($resource);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->ok(null, 'Successfully logged out.');
    }

    /**
     * Refresh a token.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function refresh(Request $request)
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * @param UserPasswordResetRequest $request
     * @return JsonResponse
     */
    public function requestNewPassword(UserPasswordResetRequest $request): JsonResponse
    {
        $emailAddress = $request->get('email');

        $user = $this->service->findUser($emailAddress);

        if (!$user) {
            return $this->notFound('No user with email ' . request('email'));
        }

        $this->service->requestResetPassword($user);

        return $this->ok(null, 'Password reset request sent. Please check your inbox.');
    }

    /**
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $token = $request->get('token');
        $password = $request->get('password');

        $user = $this->service->findUserViaResetPasswordToken($token);

        if (!$user)
            return $this->notFound('Invalid Token');

        $this->service->resetPassword($user, $password, $token);

        return $this->ok(null, 'Password reset successfully');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|string|min:6'
        ]);

        $password = $request->get('password');
        $user = $this->service->changePassword(auth()->user(), $password);

        return $this->ok(null, "Password Changed successfully");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function impersonate(Request $request): JsonResponse
    {
        $userId = $request->get('user_id');
        $user = $this->service->find($userId);

        if (auth()->user()->isAdmin()) {
            $token = JWTAuth::fromUser($user);
            return $this->respondWithToken($token);
        }

        return $this->ok(data: [], message: 'Unauthorized', statusCode: 401);
    }
}
