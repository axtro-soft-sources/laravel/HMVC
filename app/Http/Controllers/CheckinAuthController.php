<?php

namespace App\Http\Controllers;

use App\Entities\Messaging\Response;
use App\Http\Requests\ResetPasswordRequest;
use App\Services\ServiceHelper;
use App\Utils\DateUtil;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Modules\Event\Entities\SessionDefinition;
use Modules\Event\Services\SessionService;
use Modules\Event\Transformers\SessionResource;
use Modules\User\Entities\CheckinUser;
use Modules\User\Entities\UserDefinition;
use Modules\User\Http\Requests\RegisterUserRequest;
use Modules\User\Services\CheckinUserService;
use Modules\User\Services\UserService;
use Modules\User\Transformers\UserIdentityResource;
use Modules\User\Transformers\UserResource;
use Tymon\JWTAuth\Facades\JWTAuth;


class CheckinAuthController extends BaseController
{
    private $checkInUserService;


    public function __construct(CheckinUserService $checkInUserService)
    {
        $this->checkInUserService = $checkInUserService;
    }


    public function checkinUserLogin()
    {
        $credentials = request(['username', 'password']);

        $users =  CheckinUser::where([
            ['username', $credentials['username']]
        ])->get();


        foreach ($users as $user) {
            if (Hash::check($credentials['password'], $user->password)) {
                $token = JWTAuth::fromUser($user);

                if ($token) {
                    $sessions =  ($this->checkInUserService->getAllSessions($user));

                    return [
                        'sessions' => SessionResource::collection($sessions),
                        'user' => $user->username,
                        'token' => [
                            'access_token' => $token,
                            'token_type' => 'bearer',
                            'expires_in' => auth('checkin_api')->factory()->getTTL()
                        ],
                    ];

                    return $this->respondWithToken($token);
                }
            }
        }

        return response()->json(new Response(null, 'Wrong credentials.'), 422);
    }

    // me function
    public function getAuthenticatedCheckinUser()
    {
        $user = auth('checkin_api')->user();

        return $user;
    }


    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @param bool $isMobile
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->ok([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('checkin_api')->factory()->getTTL()
        ]);
    }
}
