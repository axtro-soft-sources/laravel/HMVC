<?php


namespace App;


class Roles
{
    public const SUPER_ADMIN = 'Super Admin';
    public const CUSTOMER = 'Customer';
    public const ATTENDANT = 'Attendant';
    public const BOX_OFFICE_EMPLOYEE = "Box Office Employee";
    public const ORGANIZER = "Organizer";
    public const DISTRIBUTOR = 'Distributor';
    public const External_Customer = 'External Customer';
}
