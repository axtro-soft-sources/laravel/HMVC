<?php


namespace App\Transformers;


use App\Services\ServiceHelper;
use App\Utils\ResponseUtil;
use Modules\Common\Entities\Currency;

abstract class CurrencyAwareResource extends BaseResource
{
    public function toArray($request)
    {
        $response = array_merge(parent::toArray($request), $this->_toArray($request));
        $targetCurrency = $this->displayCurrency();

        return ResponseUtil::transformCurrency($response, $targetCurrency, $this->amountFields(), $this->currencyFields());
    }

    public abstract function _toArray($request): array;

    protected function displayCurrency(): string
    {
        $targetCurrency = request()->header('X-Display-Currency', Currency::DEFAULT_CURRENCY);
        $isValidCurrency = ServiceHelper::outletService()->isSupportedCurrency($targetCurrency);
        //$isValidCurrency = in_array($targetCurrency, Meta::SUPPORTED_CURRENCIES);

        return $isValidCurrency ? $targetCurrency : Currency::DEFAULT_CURRENCY;
    }

    public abstract function amountFields(): array;

    public abstract function currencyFields(): array;
}
