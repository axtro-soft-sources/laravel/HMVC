<?php

namespace App\Transformers;

use App\Entities\BaseFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;

class BaseResource extends JsonResource
{
    public function toArray($request)
    {
        $returnValue = [];

        if ($this->resource instanceof Model) {
            if (array_key_exists(BaseFields::ID, $this->resource->getAttributes()))
                $returnValue['id'] = $this->resource[BaseFields::ID];

            if (array_key_exists(BaseFields::DEACTIVATED_AT, $this->resource->getAttributes()))
                $returnValue['is_active'] = $this->resource[BaseFields::DEACTIVATED_AT] == null;
        }

        return $returnValue;
    }

    protected function possibleDetailsRoute($request): bool
    {
        $routeName = Route::currentRouteName();
        return $routeName != null && str_ends_with($routeName, 'show');
    }
}
